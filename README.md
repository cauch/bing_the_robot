# Game:

You can choose the direction of the robot with the keyboard (arrows, asdw or qsdz keys).
The robot will then move in this direction until it hits an obstacle.
You need to escape the room.

# Dependencies:

* Python 3.4.6 (http://www.python.org/)
* PyGame 1.9.3 (http://www.pygame.org/)

# Run the game:

python run_game.py  
(run_game.pyw exists for Windows and Mac OS X, but it was not tested)

# Licence:

Code:  
Bing_the_robot is under GPL3 licence.  
Cf. COPYING

Font:  
The font Nunito is under OFL licence.
https://fonts.google.com/specimen/Nunito

Graphics:  
created for the game and are under CC BY 3.0

Sounds:  
sound effects from https://www.zapsplat.com
https://www.zapsplat.com/license-type/standard-license/

Music:  
Cycling Through Ikebukuro, by Koen Park, is under CC BY-NC-SA
http://freemusicarchive.org/music/Koen_Park/Nowhere_Home/
(converted in ogg with quality 4)
