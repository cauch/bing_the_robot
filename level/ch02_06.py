#the important variables are:
#walls: a list of 2d coord integer for the position of the walls
#p_in: a 2d coord for the initial position of the robot
#dire: initial direction of the robot (0: to the right, 1: to the left, 2: to the bottom, 3: to the top)
self.loaded["walls"]=[[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 14], [0, 15], [0, 16], [0, 17], [0, 18], [0, 19], [0, 20], [0, 21], [0, 22], [0, 23], [0, 24], [0, 25], [0, 26], [0, 27], [0, 28], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0], [12, 0], [13, 0], [14, 0], [15, 0], [16, 0], [17, 0], [18, 0], [19, 0], [20, 0], [21, 0], [22, 0], [23, 0], [24, 0], [25, 0], [26, 0], [27, 0], [28, 0], [29, 0], [30, 0], [31, 0], [32, 0], [33, 0], [34, 0], [35, 0], [36, 0], [37, 0], [38, 0], [39, 0], [39, 1], [39, 2], [39, 3], [39, 4], [39, 5], [39, 6], [39, 7], [39, 8], [39, 9], [39, 10], [39, 11], [39, 12], [39, 14], [39, 15], [39, 16], [39, 17], [39, 18], [39, 19], [39, 20], [39, 21], [39, 22], [39, 23], [39, 24], [39, 25], [39, 26], [39, 27], [39, 28], [1, 28], [2, 28], [3, 28], [4, 28], [5, 28], [6, 28], [7, 28], [8, 28], [9, 28], [10, 28], [11, 28], [12, 28], [13, 28], [14, 28], [15, 28], [16, 28], [17, 28], [18, 28], [19, 28], [20, 28], [21, 28], [22, 28], [23, 28], [24, 28], [25, 28], [26, 28], [27, 28], [28, 28], [29, 28], [30, 28], [31, 28], [32, 28], [33, 28], [34, 28], [35, 28], [36, 28], [37, 28], [38, 28], [38, 2], [1, 2], [1, 26], [38, 26], [20, 27], [19, 27], [17, 27], [22, 27], [24, 27], [15, 27], [13, 27], [26, 27], [11, 27], [28, 27], [37, 25], [2, 25], [19, 1], [20, 1], [17, 1], [22, 1], [15, 1], [24, 1], [13, 1], [26, 1], [28, 1], [11, 1], [2, 3], [37, 3], [34, 4], [8, 13], [7, 7], [7, 19], [1, 8], [6, 24], [35, 23], [34, 18], [33, 14], [32, 5], [32, 4], [36, 14], [8, 6], [9, 10], [37, 9], [1, 9], [35, 17], [3, 18], [2, 11], [8, 26], [4, 22], [37, 21], [1, 10]]
self.loaded["p_in"]=[0, 13]
self.loaded["p_out"]=[39, 13]
self.loaded["length"]=19
self.loaded["dire"]=0
self.loaded["chapter"] = "Chapter 2: the cave"
self.loaded["level"] = "Level 6/6"
self.loaded["intro"] = "Hey, I remember this room pattern, and ... Oh my! Is this ...\nYes! Those are robots! Here! This planet is supposed to be uninhabited. What the hell!?\nBetter not approach"
self.loaded["special"] = []
self.loaded["special"].append(["en_r",[18,19],[18,27],[18,1]])
self.loaded["special"].append(["en_r",[21,10],[21,27],[21,1]])
self.loaded["special"].append(["en_r",[23,1],[23,27]])
cm = self.display.module["play"]
cm.next_level = "ch03_cs"
cm.image_bloc = ["bloc_metal3","bloc_metal4"]
cm.image_tile = ["tile_grey1","tile_grey2","tile_grey3"]
cm.robottext_cl = (230,100,0)
