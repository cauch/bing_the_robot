#the important variables are:
#walls: a list of 2d coord integer for the position of the walls
#p_in: a 2d coord for the initial position of the robot
#dire: initial direction of the robot (0: to the right, 1: to the left, 2: to the bottom, 3: to the top)
self.loaded["walls"]=[[0, 0], [0, 1], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13], [0, 14], [0, 15], [0, 16], [0, 17], [0, 18], [0, 19], [0, 20], [0, 21], [0, 22], [0, 23], [0, 24], [0, 25], [0, 26], [0, 27], [0, 28], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0], [12, 0], [13, 0], [14, 0], [15, 0], [16, 0], [17, 0], [18, 0], [19, 0], [20, 0], [21, 0], [22, 0], [23, 0], [24, 0], [25, 0], [26, 0], [27, 0], [28, 0], [29, 0], [30, 0], [31, 0], [32, 0], [33, 0], [34, 0], [35, 0], [36, 0], [37, 0], [38, 0], [39, 0], [39, 1], [39, 2], [39, 3], [39, 4], [39, 5], [39, 6], [39, 7], [39, 8], [39, 9], [39, 10], [39, 11], [39, 12], [39, 13], [39, 14], [39, 15], [39, 16], [39, 17], [39, 18], [39, 19], [39, 20], [39, 22], [39, 23], [39, 24], [39, 25], [39, 26], [39, 27], [39, 28], [1, 28], [2, 28], [3, 28], [4, 28], [5, 28], [6, 28], [7, 28], [8, 28], [9, 28], [10, 28], [11, 28], [12, 28], [13, 28], [14, 28], [15, 28], [16, 28], [17, 28], [18, 28], [19, 28], [20, 28], [21, 28], [22, 28], [23, 28], [24, 28], [25, 28], [26, 28], [27, 28], [28, 28], [29, 28], [30, 28], [31, 28], [32, 28], [33, 28], [34, 28], [35, 28], [36, 28], [37, 28], [38, 28], [0, 2], [29, 3], [7, 1], [8, 4], [6, 4], [28, 9], [34, 8], [33, 14], [23, 13], [24, 8], [10, 9], [11, 13], [4, 12], [5, 18], [11, 17], [10, 25], [18, 24], [17, 19], [23, 20], [34, 23], [33, 20], [24, 17], [35, 21], [36, 22], [35, 24], [37, 23], [38, 20], [38, 25], [30, 26], [31, 18], [38, 19], [34, 24], [37, 25], [33, 24], [35, 27], [33, 27], [22, 26], [27, 25], [26, 22]]
self.loaded["p_in"]=[0, 3]
self.loaded["p_out"]=[39, 21]
self.loaded["length"]=31
self.loaded["dire"]=0
self.loaded["chapter"] = "Chapter 3: the lab"
self.loaded["level"] = "Level 3/6"
self.loaded["intro"] = "Oh dear. Another kind of robot. Flying above the walls.\nAnd it looks like it's heading in your direction."
self.loaded["special"] = []
self.loaded["special"].append(["en_d",[19,13]])
cm = self.display.module["play"]
cm.next_level = "ch03_04"
cm.image_bloc = ["bloc_metal4","bloc_metal2","bloc_metal2","bloc_metal2","bloc_metal2"]
cm.image_tile = ["tile_grey1","tile_grey2","tile_grey3"]
cm.robottext_cl = (230,100,0)
