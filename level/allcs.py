#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
cm.next_level = "ch01_01"

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"The rover is busted.","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"When moving, it is not able to turn or even stop.","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":550,"h":100,"lh":50,"ld":50,"p":[150,95]})
sc.append({"?":"text","text":"Well, I guess we had a pretty good run exploring this planet.","p":[170,115],"f":"18"})
sc.append({"?":"text","text":"And now it appears to be stuck into those rocks.","p":[170,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"The mission is finally over.","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"Let's just get it out of these rocks and call it a day.","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
cm.next_level = "ch02_01"

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Wow! What is this?!","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"Is this a cave? We never noticed it before.","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":450,"h":70,"lh":50,"ld":50,"p":[150,125]})
sc.append({"?":"text","text":"I guess we will explore a little bit further, then.","p":[170,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":450,"h":70,"lh":50,"ld":50,"p":[150,125]})
sc.append({"?":"text","text":"Here goes my evening watching Netflix!","p":[170,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"You should have thought about that before becoming","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"a scientific assistant.","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
cm.next_level = "ch03_01"

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":-40,"p":[110,95]})
sc.append({"?":"text","text":"Wow! This is incredible! Robots!","p":[140,115],"f":"18"})
sc.append({"?":"text","text":"This is the discovery of the century!","p":[140,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"When I think we almost stopped the mission before","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"finding this!","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":200,"h":70,"lh":50,"ld":-30,"p":[150,115]})
sc.append({"?":"text","text":"Extraordinary!","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":450,"h":70,"lh":50,"ld":50,"p":[150,125]})
sc.append({"?":"text","text":"Meh! I still think alien robots look better on Netflix!","p":[170,145],"f":"18"})
cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
cm.next_level = "!end"

w,h = self.display.screen_width,self.display.screen_height
#sc = []
#sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
#sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
#sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":-40,"p":[110,95]})
#sc.append({"?":"text","text":"Wow! This is incredible! Robots!","p":[140,115],"f":"18"})
#sc.append({"?":"text","text":"This is the discovery of the century!","p":[140,145],"f":"18"})
#cm.scene_list.append(sc)
#sc = []
#sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
#sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
#sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
#sc.append({"?":"text","text":"When I think we almost stopped the mission before","p":[120,115],"f":"18"})
#sc.append({"?":"text","text":"finding this!","p":[120,145],"f":"18"})
#cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-25,h-300]})
sc.append({"?":"bubble","w":200,"h":70,"lh":50,"ld":-30,"p":[150,115]})
sc.append({"?":"text","text":"But but but!","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":240,"h":70,"lh":50,"ld":-30,"p":[150,115]})
sc.append({"?":"text","text":"I don't understand!","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist4","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":50,"p":[w-370,115]})
sc.append({"?":"text","text":"Hey guys!","p":[w-350,135],"f":"18"})
sc.append({"?":"text","text":"I see you've got out of the set.","p":[w-350,165],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":130,"h":70,"lh":50,"ld":-20,"p":[150,115]})
sc.append({"?":"text","text":"The set?","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist4","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":200,"h":70,"lh":50,"ld":50,"p":[w-370+40,125]})
sc.append({"?":"text","text":"Yes, the studio set.","p":[w-350+40,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":360,"h":70,"lh":50,"ld":-40,"p":[150,115]})
sc.append({"?":"text","text":"What? We are not in another planet?","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist4","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":480,"h":100,"lh":50,"ld":60,"p":[w-370-180,115]})
sc.append({"?":"text","text":"Of course not! Do you know how much","p":[w-350-180,135],"f":"18"})
sc.append({"?":"text","text":"a space rocket cost? It's easier to just rent a set.","p":[w-350-180,165],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist4","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":480,"h":70,"lh":50,"ld":60,"p":[w-370-180,125]})
sc.append({"?":"text","text":"Did you not noticed that? I thought everybody knew.","p":[w-350-180,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":300,"h":70,"lh":50,"ld":-40,"p":[150,115]})
sc.append({"?":"text","text":"But! The cave? The secret lab?","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist4","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":540,"h":100,"lh":50,"ld":60,"p":[w-370-220,115]})
sc.append({"?":"text","text":"You mean the storage rooms? Oh, it's a real mess!","p":[w-350-220,135],"f":"18"})
sc.append({"?":"text","text":"Plenty of drones and roomba cleaning robots got lost there.","p":[w-350-220,165],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":130,"h":70,"lh":50,"ld":-20,"p":[150,115]})
sc.append({"?":"text","text":"But why?","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist4","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":560,"h":140,"lh":50,"ld":80,"p":[w-370-240,105]})
sc.append({"?":"text","text":"Oh, you know how the world's leaders are! They are behind","p":[w-350-240,115],"f":"18"})
sc.append({"?":"text","text":"everything. The people are happy with fake space missions, and","p":[w-350-240,145],"f":"18"})
sc.append({"?":"text","text":"the scientists are busy and don't bother them by pointing out","p":[w-350-240,175],"f":"18"})
sc.append({"?":"text","text":"the real problems.","p":[w-350-240,205],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":100,"h":70,"lh":50,"ld":-10,"p":[150,115]})
sc.append({"?":"text","text":"Oh!","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":400,"h":70,"lh":50,"ld":-50,"p":[150,115]})
sc.append({"?":"text","text":"Well! Let's go home and watch Netflix.","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[110,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-400,h-300]})
sc.append({"?":"img","n":"scientist3","p":[w-120,h-300]})
sc.append({"?":"bubble","w":130,"h":70,"lh":50,"ld":10,"p":[360,115]})
sc.append({"?":"text","text":"Yeaaaah!","p":[380,135],"f":"18"})
cm.scene_list.append(sc)






