#the important variables are:
#walls: a list of 2d coord integer for the position of the walls
#p_in: a 2d coord for the initial position of the robot
#dire: initial direction of the robot (0: to the right, 1: to the left, 2: to the bottom, 3: to the top)

self.loaded["walls"] = [[0, 1], [0, 2], [0, 7], [0, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 14], [0, 19], [0, 20], [0, 21], [0, 22], [0, 23], [0, 26], [0, 27], [1, 0], [3, 0], [5, 0], [6, 0], [8, 0], [9, 0], [11, 0], [12, 0], [13, 0], [14, 0], [16, 0], [18, 0], [19, 0], [20, 0], [22, 0], [23, 0], [24, 0], [25, 0], [29, 0], [31, 0], [33, 0], [35, 0], [36, 0], [38, 0], [39, 1], [39, 2], [39, 7], [39, 8], [39, 9], [39, 10], [39, 11], [39, 12], [39, 14], [39, 19], [39, 20], [39, 21], [39, 22], [39, 23], [39, 24], [39, 25], [39, 26], [39, 27], [1, 28], [3, 28], [4, 28], [6, 28], [7, 28], [9, 28], [10, 28], [11, 28], [13, 28], [15, 28], [16, 28], [18, 28], [19, 28], [21, 28], [22, 28], [23, 28], [25, 28], [26, 28], [28, 28], [30, 28], [31, 28], [32, 28], [33, 28], [35, 28], [38, 28], [2, 1], [2, 27], [37, 27], [37, 1], [5, 13], [5, 27], [3, 8], [4, 1], [8, 9], [8, 27], [7, 1], [12, 12], [12, 27], [10, 1], [14, 8], [15, 1], [14, 27], [13, 21], [19, 20], [17, 27], [20, 27], [21, 1], [20, 11], [24, 27], [26, 1], [29, 27], [30, 1], [33, 11], [32, 1], [36, 27], [35, 12], [34, 13], [28, 1], [25, 25], [25, 9], [29, 8], [24, 24], [30, 23], [33, 9]]
self.loaded["p_in"] = [0, 13]
self.loaded["p_out"]=[39, 13]
self.loaded["length"]=33
self.loaded["dire"] = 0
self.loaded["chapter"] = "Chapter 1: tutorial"
self.loaded["level"] = "Level 3/6"
self.loaded["intro"] = "In this level, try to not get stuck"
cm = self.display.module["play"]
cm.next_level = "ch01_04"
cm.image_bloc = ["bloc_rock1%i"%x for x in range(4)]
cm.image_bloc += ["bloc_rock3%i"%x for x in range(4)]
cm.image_tile = ["tile_dirt02a%i"%x for x in range(4)]
cm.image_tile += ["tile_dirt02b%i"%x for x in range(4)]
#cm.image_tile += ["tile_dirt02c%i"%x for x in range(4)]
cm.image_tile += ["tile_dirt01a%i"%x for x in range(4)]
cm.image_tile += ["tile_dirt01b%i"%x for x in range(4)]
#cm.image_tile += ["tile_dirt01c%i"%x for x in range(4)]
cm.robottext_cl = (100,50,0)
