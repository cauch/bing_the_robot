#the important variables are:
#walls: a list of 2d coord integer for the position of the walls
#p_in: a 2d coord for the initial position of the robot
#dire: initial direction of the robot (0: to the right, 1: to the left, 2: to the bottom, 3: to the top)
self.loaded["walls"]=[[14, 21], [38, 22], [37, 25], [21, 24], [22, 13], [12, 14], [13, 9], [29, 10], [28, 12], [4, 11], [5, 4], [0, 0], [0, 1], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 14], [0, 15], [0, 16], [0, 21], [0, 23], [0, 24], [0, 25], [0, 26], [0, 27], [0, 28], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0], [14, 0], [15, 0], [16, 0], [17, 0], [22, 0], [23, 0], [24, 0], [25, 0], [26, 0], [27, 0], [28, 0], [32, 0], [33, 0], [34, 0], [35, 0], [37, 0], [38, 0], [39, 0], [39, 1], [39, 2], [39, 6], [39, 7], [39, 8], [39, 9], [39, 11], [39, 12], [39, 13], [39, 14], [39, 15], [39, 16], [39, 17], [39, 21], [39, 22], [39, 23], [39, 24], [39, 25], [39, 26], [39, 27], [39, 28], [1, 28], [2, 28], [3, 28], [4, 28], [5, 28], [6, 28], [7, 28], [8, 28], [9, 28], [10, 28], [11, 28], [12, 28], [13, 28], [15, 28], [16, 28], [17, 28], [18, 28], [20, 28], [21, 28], [22, 28], [23, 28], [24, 28], [25, 28], [26, 28], [27, 28], [33, 28], [34, 28], [35, 28], [36, 28], [37, 28], [38, 28], [2, 10], [5, 23], [34, 11], [37, 15], [22, 27], [9, 22], [1, 5], [6, 27], [28, 7], [15, 25], [38, 25], [38, 23], [36, 13], [23, 21], [30, 4], [35, 21], [14, 15], [24, 25], [21, 13], [14, 12], [27, 12], [26, 6], [27, 9], [7, 16], [5, 1], [35, 2], [4, 1], [1, 1], [20, 9], [0, 22], [0, 17], [28, 28], [13, 0]]
self.loaded["p_in"]=[14, 28]
self.loaded["p_out"]=[39, 5]
self.loaded["length"]=12
self.loaded["dire"]=3
self.loaded["chapter"] = "Chapter 1: tutorial"
self.loaded["level"] = "Level 5/6"
self.loaded["intro"] = "The game is more complicated when you don't know where is the way out"
cm = self.display.module["play"]
cm.next_level = "ch01_06"
cm.image_bloc = ["bloc_rock1%i"%x for x in range(4)]
cm.image_bloc += ["bloc_rock3%i"%x for x in range(4)]
cm.image_tile = ["tile_dirt01a%i"%x for x in range(4)]
cm.image_tile += ["tile_dirt02a%i"%x for x in range(4)]
cm.image_tile += ["tile_dirt03a%i"%x for x in range(4)]
cm.image_tile += ["tile_dirt04a%i"%x for x in range(4)]
cm.robottext_cl = (230,100,0)
