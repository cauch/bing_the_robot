#the important variables are:
#walls: a list of 2d coord integer for the position of the walls
#p_in: a 2d coord for the initial position of the robot
#dire: initial direction of the robot (0: to the right, 1: to the left, 2: to the bottom, 3: to the top)
self.loaded["walls"]=[[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13], [0, 14], [0, 15], [0, 16], [0, 17], [0, 18], [0, 19], [0, 21], [0, 22], [0, 23], [0, 24], [0, 25], [0, 26], [0, 27], [0, 28], [1, 28], [2, 28], [3, 28], [4, 28], [5, 28], [6, 28], [7, 28], [8, 28], [9, 28], [10, 28], [12, 28], [11, 28], [13, 28], [1, 0], [3, 0], [2, 0], [4, 0], [5, 0], [7, 0], [8, 0], [6, 0], [9, 0], [11, 0], [12, 0], [10, 0], [13, 0], [14, 28], [6, 20], [5, 25], [5, 10], [3, 7], [12, 14], [39, 1], [39, 7], [39, 11], [39, 12], [39, 14], [39, 23], [39, 27], [7, 11], [9, 19], [8, 24], [8, 2], [35, 3], [4, 18], [37, 17], [34, 8], [36, 13], [13, 20], [34, 19], [33, 10], [10, 11], [11, 4], [38, 0], [34, 0], [38, 28], [36, 28], [33, 28], [37, 5], [35, 23], [39, 20], [34, 28], [14, 0]]
self.loaded["p_in"]= [0,20]
self.loaded["p_out"]=[36, 0]
self.loaded["length"]=20
self.loaded["dire"]=0
self.loaded["chapter"] = "Chapter 3: the lab"
self.loaded["level"] = "Level 6/6"
self.loaded["intro"] = "Finally outside.\nUh? What???"
self.loaded["special"] = []
self.loaded["bkgd_right"] = ["tile_ground01a%i"%i for i in range(4)]
self.loaded["bkgd_right"] += ["tile_ground02a%i"%i for i in range(4)]
self.loaded["bkgd_rect"] = []
self.loaded["bkgd_rect"].append([(150,150,150),500,20,60+60+20,580])
self.loaded["bkgd_rect"].append([(250,250,250),565,20,10,100])
self.loaded["bkgd_rect"].append([(250,250,250),565,220,10,100])
self.loaded["bkgd_rect"].append([(250,250,250),565,420,10,100])

self.loaded["special"].append(["en_c1",[25,-4],[25,10],[25,41],[29,41],[29,-4]])
self.loaded["special"].append(["en_c2",[25,10],[25,41],[29,41],[29,-4],[25,-4]])
self.loaded["special"].append(["en_c3",[29,41],[29,-4],[25,-4],[25,10],[25,41]])
self.loaded["special"].append(["en_c4",[29,23],[29,-4],[25,-4],[25,41],[29,41]])
self.loaded["special"].append(["en_c5",[29,13],[29,-4],[25,-4],[25,41],[29,41]])
#self.loaded["special"].append(["en_c5",[29,3],[29,-4],[25,-4],[25,41],[29,41]])

cm = self.display.module["play"]
cm.next_level = "ch03_fi"
cm.image_bloc = ["bloc_brick"]
cm.image_tile = ["tile_grey1","tile_grey2","tile_grey3"]
cm.robottext_cl = (230,100,0)
