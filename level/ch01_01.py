#the important variables are:
#walls: a list of 2d coord integer for the position of the walls
#p_in: a 2d coord for the initial position of the robot
#dire: initial direction of the robot (0: to the right, 1: to the left, 2: to the bottom, 3: to the top)

self.loaded["walls"] = [[0, 1], [0, 11], [1, 0], [6, 0], [39, 5], [39, 13], [39, 15], [13, 12], [14, 13], [1, 12], [1, 14], [13, 20], [5, 20], [4, 19], [5, 18], [24, 19], [23, 11], [14, 11], [25, 12], [6, 20], [6, 12], [24, 4], [38, 6], [5, 5], [7, 1], [13, 7], [33, 8], [32, 7], [32, 15], [25, 18], [0, 8], [23, 28], [38, 4], [24, 27], [1, 28], [0, 27], [1, 26], [31, 13], [30, 14], [30, 27], [31, 28], [39, 27], [38, 28], [38, 26]]
self.loaded["p_in"] = [0, 13]
self.loaded["dire"] = 0
self.loaded["p_out"]=[39, 14]
self.loaded["length"]=13
self.loaded["chapter"] = "Chapter 1: tutorial"
self.loaded["level"] = "Level 1/6"
self.loaded["intro"] = "You don't risk anything here\nJust use the arrow keys and get out of the screen\nDuring game, use 'esc' or 'p' to pause"
cm = self.display.module["play"]
cm.next_level = "ch01_02"
cm.image_bloc = ["bloc_rock1%i"%x for x in range(4)]
cm.image_tile = ["tile_dirt02a%i"%x for x in range(4)]
cm.image_tile += ["tile_dirt02b%i"%x for x in range(4)]
cm.image_tile += ["tile_dirt02c%i"%x for x in range(4)]
cm.robottext_cl = (50,0,0)
