#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
cm.next_level = "ch02_01"

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Wow! What is this?!","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"Is this a cave? We never noticed it before.","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":450,"h":70,"lh":50,"ld":50,"p":[150,125]})
sc.append({"?":"text","text":"I guess we will explore a little bit further, then.","p":[170,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":450,"h":70,"lh":50,"ld":50,"p":[150,125]})
sc.append({"?":"text","text":"Here goes my evening watching Netflix!","p":[170,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"You should have thought about that before becoming","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"a scientific assistant.","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
