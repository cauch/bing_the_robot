#the important variables are:
#walls: a list of 2d coord integer for the position of the walls
#p_in: a 2d coord for the initial position of the robot
#dire: initial direction of the robot (0: to the right, 1: to the left, 2: to the bottom, 3: to the top)
self.loaded["walls"]=[[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 14], [0, 15], [0, 16], [0, 17], [0, 18], [0, 19], [0, 20], [0, 21], [0, 22], [0, 23], [0, 24], [0, 25], [0, 26], [0, 27], [0, 28], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0], [12, 0], [13, 0], [14, 0], [15, 0], [16, 0], [17, 0], [18, 0], [19, 0], [20, 0], [21, 0], [22, 0], [23, 0], [24, 0], [25, 0], [26, 0], [27, 0], [28, 0], [29, 0], [30, 0], [31, 0], [32, 0], [33, 0], [34, 0], [35, 0], [36, 0], [37, 0], [38, 0], [39, 0], [39, 1], [39, 2], [39, 3], [39, 4], [39, 5], [39, 6], [39, 7], [39, 8], [39, 9], [39, 10], [39, 11], [39, 12], [39, 13], [39, 14], [39, 15], [39, 16], [39, 17], [39, 18], [39, 19], [39, 20], [39, 21], [39, 23], [39, 24], [39, 25], [39, 26], [39, 27], [39, 28], [1, 28], [2, 28], [3, 28], [4, 28], [5, 28], [6, 28], [7, 28], [8, 28], [9, 28], [10, 28], [11, 28], [12, 28], [13, 28], [14, 28], [15, 28], [16, 28], [17, 28], [18, 28], [19, 28], [20, 28], [21, 28], [22, 28], [23, 28], [24, 28], [25, 28], [26, 28], [27, 28], [28, 28], [29, 28], [30, 28], [31, 28], [32, 28], [33, 28], [34, 28], [35, 28], [36, 28], [37, 28], [38, 28], [16, 1], [16, 2], [16, 3], [16, 4], [16, 5], [16, 6], [16, 7], [16, 9], [16, 11], [16, 10], [16, 12], [16, 13], [16, 15], [16, 14], [16, 16], [15, 16], [13, 16], [14, 16], [12, 16], [11, 16], [1, 16], [2, 16], [3, 16], [5, 16], [4, 16], [6, 16], [8, 16], [9, 16], [7, 16], [17, 16], [18, 16], [20, 16], [19, 16], [21, 16], [22, 16], [23, 16], [24, 16], [25, 16], [28, 16], [27, 16], [29, 16], [30, 16], [32, 16], [31, 16], [34, 16], [33, 16], [35, 16], [36, 16], [37, 16], [38, 23], [20, 17], [20, 18], [20, 19], [20, 20], [20, 21], [20, 22], [20, 24], [20, 25], [20, 26], [20, 27], [11, 13], [10, 20], [18, 27], [18, 17], [15, 14], [14, 13], [1, 14], [10, 7], [15, 19], [6, 19], [7, 21], [9, 24], [30, 23], [29, 19], [29, 26], [25, 24], [24, 20], [25, 17], [21, 18], [25, 15], [24, 14], [38, 2], [26, 8], [21, 3], [5, 4], [4, 8], [15, 4], [13, 12], [25, 1], [37, 13], [36, 14], [36, 12], [37, 9], [36, 5], [37, 6], [26, 6], [36, 15], [37, 3], [27, 15], [21, 5], [36, 17], [36, 18], [36, 19], [37, 20], [21, 22], [32, 17], [36, 21], [5, 13], [7, 9], [6, 14]]
self.loaded["p_in"]=[0, 13]
self.loaded["p_out"]=[39, 22]
self.loaded["length"]=21
self.loaded["dire"]=0
self.loaded["chapter"] = "Chapter 3: the lab"
self.loaded["level"] = "Level 2/6"
self.loaded["intro"] = "Now you have to exit 4 rooms, and avoid the robots."
self.loaded["special"] = []
self.loaded["special"].append(["en_r",[10,19],[10,8],[25,8],[25,9],[26,9],[26,23],[11,23],[11,19] ])
self.loaded["special"].append(["en_r",[22,23],[11,23],[11,19],[10,19],[10,8],[25,8],[25,9],[26,9],[26,23] ])
self.loaded["special"].append(["en_r",[10,19],[11,19],[11,23],[26,23],[26,9],[25,9],[25,8],[10,8] ])
#self.loaded["special"].append(["en_r",[20,23],[26,23],[26,9],[25,9],[25,8],[10,8],[10,19],[11,19],[11,23] ])
#self.loaded["special"].append(["en_r",[26,15],[26,23],[11,23],[11,19],[10,19],[10,8],[25,8],[25,9],[26,9] ])
cm = self.display.module["play"]
cm.next_level = "ch03_03"
cm.image_bloc = ["bloc_metal1","bloc_metal2"]
cm.image_tile = ["tile_grey1","tile_grey2","tile_grey3"]
cm.robottext_cl = (230,100,0)
