#the important variables are:
#walls: a list of 2d coord integer for the position of the walls
#p_in: a 2d coord for the initial position of the robot
#dire: initial direction of the robot (0: to the right, 1: to the left, 2: to the bottom, 3: to the top)
self.loaded["walls"]=[[9, 7], [8, 17], [12, 16], [11, 26], [20, 25], [19, 13], [30, 14], [29, 23], [35, 22], [34, 3], [27, 4], [0, 0], [0, 1], [0, 3], [0, 4], [0, 6], [0, 8], [0, 9], [0, 10], [0, 11], [0, 12], [0, 13], [0, 14], [0, 15], [0, 16], [0, 17], [0, 19], [0, 20], [0, 21], [0, 22], [0, 23], [0, 24], [0, 25], [0, 26], [0, 27], [0, 28], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0], [12, 0], [13, 0], [15, 0], [16, 0], [17, 0], [18, 0], [19, 0], [20, 0], [21, 0], [22, 0], [23, 0], [24, 0], [25, 0], [26, 0], [27, 0], [28, 0], [29, 0], [30, 0], [31, 0], [32, 0], [33, 0], [34, 0], [35, 0], [36, 0], [37, 0], [38, 0], [39, 0], [39, 1], [39, 2], [39, 3], [39, 4], [39, 5], [39, 6], [39, 7], [39, 8], [39, 9], [39, 10], [39, 11], [39, 12], [39, 13], [39, 14], [39, 15], [39, 16], [39, 17], [39, 18], [39, 19], [39, 20], [39, 21], [39, 22], [39, 23], [39, 24], [39, 25], [39, 26], [39, 27], [39, 28], [1, 28], [2, 28], [3, 28], [4, 28], [5, 28], [7, 28], [8, 28], [10, 28], [11, 28], [12, 28], [13, 28], [15, 28], [16, 28], [17, 28], [18, 28], [19, 28], [20, 28], [21, 28], [22, 28], [23, 28], [25, 28], [26, 28], [27, 28], [29, 28], [30, 28], [31, 28], [32, 28], [33, 28], [34, 28], [35, 28], [36, 28], [37, 28], [38, 28], [8, 2], [34, 26], [29, 11], [1, 25], [37, 4], [11, 3], [1, 14], [10, 5], [27, 10], [7, 6], [35, 10], [9, 5], [18, 26], [6, 8], [5, 13], [13, 26], [4, 4], [3, 22], [18, 23], [13, 13], [33, 3], [33, 23], [12, 22], [2, 13], [38, 2], [37, 3], [2, 26], [2, 16], [14, 0], [7, 22], [0, 18], [26, 5], [25, 6], [24, 28], [0, 2], [0, 5], [14, 28], [6, 28], [9, 28],[28,28]]
self.loaded["p_in"]=[0, 7]
self.loaded["p_out"]=[28, 28]
self.loaded["length"]=12
self.loaded["dire"]=0
self.loaded["chapter"] = "Chapter 2: the cave"
self.loaded["level"] = "Level 5/6"
self.loaded["intro"] = "Now, it's the exits that open and close. Which one is the correct one?"
self.loaded["special"] = []
self.loaded["special"].append(["blink",[28,28]])
self.loaded["special"].append(["blink",[14,0]])
self.loaded["special"].append(["blink",[20,0]])
self.loaded["special"].append(["blink",[0,18]])
self.loaded["special"].append(["blink",[39,9]])
self.loaded["special"].append(["blink",[24,28]])
self.loaded["special"].append(["blink",[0,5]])
self.loaded["special"].append(["blink",[9,28]])
cm = self.display.module["play"]
cm.next_level = "ch02_06"
cm.image_bloc = ["bloc_metal3","bloc_metal4","bloc_metal1"]
cm.image_tile = ["tile_grey1","tile_grey2","tile_grey3"]
cm.robottext_cl = (230,100,0)
