#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
cm.next_level = "ch03_01"

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":-40,"p":[110,95]})
sc.append({"?":"text","text":"Wow! This is incredible! Robots!","p":[140,115],"f":"18"})
sc.append({"?":"text","text":"This is the discovery of the century!","p":[140,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"When I think we almost stopped the mission before","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"finding this!","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":200,"h":70,"lh":50,"ld":-30,"p":[150,115]})
sc.append({"?":"text","text":"Extraordinary!","p":[180,135],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":450,"h":70,"lh":50,"ld":50,"p":[150,125]})
sc.append({"?":"text","text":"Meh! I still think alien robots look better on Netflix!","p":[170,145],"f":"18"})
cm.scene_list.append(sc)
