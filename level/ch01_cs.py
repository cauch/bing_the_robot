#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
cm.next_level = "ch01_01"

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"The rover is busted.","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"When moving, it is not able to turn or even stop.","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":550,"h":100,"lh":50,"ld":50,"p":[150,95]})
sc.append({"?":"text","text":"Well, I guess we had a pretty good run exploring this planet.","p":[170,115],"f":"18"})
sc.append({"?":"text","text":"And now it appears to be stuck into those rocks.","p":[170,145],"f":"18"})
cm.scene_list.append(sc)
sc = []
sc.append({"?":"img","n":"scientist1","p":[120,h-300]})
sc.append({"?":"img","n":"scientist2","p":[w-300,h-300]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"The mission is finally over.","p":[120,115],"f":"18"})
sc.append({"?":"text","text":"Let's just get it out of these rocks and call it a day.","p":[120,145],"f":"18"})
cm.scene_list.append(sc)
