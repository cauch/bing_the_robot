#! /usr/bin/env python3

#    This file is part of Bing_the_robot.
#
#    Bing_the_robot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Bing_the_robot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Bing_the_robot.  If not, see <http://www.gnu.org/licenses/>.

from game.btr_play import Robot,Wall,Enemy
from game.btr_generator import Generator
from game.btr_profile import Profile
import random
import os

class Core:
    def __init__(self):
        self.display = None
        self.play = 1       #0:quit, 1:play
        self.time = 0        #time in iteration of the while loop
        self.fps = 40        #fps used in display -> 1 iteration = 1./fps seconds
        self.robot = None
        self.walls = []       #list of the objects
        self.walls_per_x = {} #list of the objects, but per "x" wall position
        self.walls_per_y = {} #list of the objects, but per "x" wall position
        self.walls_coord = [] #list of the [x,y] of the objects

        self.grid_size = None
        self.latest_score = None

        self.loaded = {}

        self.profile = Profile(self)
        self.profile.load()

    def load(self):
        #initialize Display
        self.display.init()
        if self.display.mode == "play":
            self.load_play()
        if self.display.mode == "menu":
            self.load_menu()
        #main loop
        self.run()

    def load_menu(self):
        cm = self.display.module["menu"]
        self.display.mode = "menu"
        cm.init()

    def new_game(self):
        self.walls = []
        self.walls_per_x = {}
        self.walls_per_y = {}
        self.walls_coord = []
        mode = self.display.mode
        cm = self.display.module[mode]
        cm.end()
        self.display.mode = "play"
        self.load_play()

    def back_to_menu(self):
        mode = self.display.mode
        cm = self.display.module[mode]
        cm.end()
        self.load_menu()

    def load_cutscene(self):
        mode = self.display.mode
        cm = self.display.module[mode]
        cm.end()
        self.display.mode = "cutscene"
        cm = self.display.module["cutscene"]
        cm.init()
        if not cm.next_level.startswith("random") and not cm.next_level.startswith("!"):
            self.profile.lastlevel = cm.next_level
            self.profile.save()

    def load_play(self):
        cm = self.display.module["play"]
        cm.init()
        self.grid_size = [cm.grid_width,cm.grid_height]
        #initialize Walls
        self.loaded = {}
        if cm.level_name.startswith("!"):
            #special case
            if cm.level_name == "!scorerandom":
                cm.level_name = "random"
                if self.latest_score != None:
                    self.loaded["intro"] = "You've got a score of %i"%self.latest_score
                else:
                    self.loaded["intro"] = ""
                self.loaded["intro"] += "\nNext level: randomly generated"
            if cm.level_name == "!end":
                self.loaded["intro"] = "Congratulations! You've finished the game!"
                self.loaded["intro"] += "\nYour final score is %i"%self.profile.currentscore
                self.loaded["p_in"] = [-10,-10]
                self.loaded["walls"] = [[0,0]]
                cm.next_level = "!menu"
        if cm.level_name.startswith("random"):
            g = Generator()
            self.loaded["levelname"] = cm.level_name
            cm.next_level = "!scorerandom"
            self.loaded["chapter"] = ""
            self.loaded["level"] = "Randomly generated map"
            if self.loaded.get("intro","") == "":
                self.loaded["intro"] = "This level is randomly generated."
            if len(cm.level_name.split("_"))==2:
                g.set_seed(int(cm.level_name.split("_")[1]))
            g.set_size(self.grid_size[0],self.grid_size[1])
            g.set_number(3,7)
            i=0
            while i==0:
                i = g.generate()
                if i:
                    self.loaded["p_in"] = [g.p_in[0],g.p_in[1]]
                    self.loaded["walls"] = g.wall
                    self.loaded["dire"] = g.p_in_dire
                    r = g.solve(g.wall,g.p_in,g.p_out)
                    self.loaded["length"] = g.s_length
                    if g.s_length <= 0:
                        i = 0
        elif not cm.level_name.startswith("!"):
            self.load_level(cm.level_name)
            if self.latest_score != None:
                self.loaded["lastscore"] = "You just got %i points, "%self.latest_score
                self.loaded["lastscore"] += "for a total of %i\n\n"%self.profile.currentscore

        if self.loaded.get("cutscene",0):
            self.load_cutscene()
        elif cm.level_name == "!menu":
            self.back_to_menu()
        else:
            for p in self.loaded.get("walls",[]):
                self.add_wall(p[0],p[1])
            dire = self.loaded.get("dire",0)
            #add the entrance wall
            p_in = self.loaded.get("p_in",[0,0])
            self.add_wall(p_in[0],p_in[1],1)
            #special things
            for p in self.loaded.get("special",[]):
                self.set_special(p)
            #initialize Robot
            self.robot = Robot(self)
            self.robot.place_next(p_in,self.robot.size,0,0)
            cm.layer1_obj.append(self.robot)
            self.robot.nr = 1
            cm.move_robot([[5,0],[-5,0],[0,5],[0,-5]][dire])
            intro = self.loaded.get("lastscore","")
            if intro:
                self.latest_score = None
            if self.loaded.get("chapter","") or self.loaded.get("level",""):
                intro += "Next level:\n"
                if self.loaded.get("chapter","") and self.loaded.get("level",""):
                    intro += self.loaded.get("chapter","")+",  "+self.loaded.get("level","")
                else:
                    if self.loaded.get("chapter",""):
                        intro += self.loaded.get("chapter","")
                    if self.loaded.get("level",""):
                        intro += self.loaded.get("level","")
            intro += "\n"+self.loaded.get("intro","")
            intro += "\n\n!Sclick on any key to continue"
            cm.add_curtain(intro)

    def load_level(self,level_name):
        self.loaded["levelname"] = level_name
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","level","%s.py"%level_name)
        if os.path.exists(filename):
            exec(open(filename).read())
        else:
            raise(SystemExit, 'level file not found: %s'%level_name)

    def set_special(self,p):
        if p[0] == "inv":
            #find the wall with these coordinated and give it the "invisible" property
            lw = [w for w in self.walls if w.pos == p[1]]
            if len(lw) == 1:
                lw[0].properties.append("inv")
                lw[0].properties_t.append(51-random.randint(0,5))
            else:
                print("not found",p,lw)
        elif p[0] == "blink":
            #find the wall with these coordinated and give it the "invisible" property
            lw = [w for w in self.walls if w.pos == p[1]]
            if len(lw) == 1:
                lw[0].properties.append("blink")
                lw[0].properties_t.append(1100+random.randint(0,100))
            else:
                print("not found",p,lw)
        elif p[0] in ["en_r","en_d","en_c1","en_c2","en_c3","en_c4","en_c5"]:
            cm = self.display.module["play"]
            e = Enemy(self)
            e.image_name = p[0]
            e.param = p[1:]
            e.pos = [p[1][0]*e.size,(p[1][1]*e.size)+cm.screen_topbar]
            cm.layer2_obj.append(e)

    def add_wall(self,x,y,tr=0,w=None):
        if tr != 2:
            w = Wall(self)
            w.pos = [x,y]
        if tr != 1:
            self.walls.append(w)
            self.walls_coord.append([x,y])
            if x not in self.walls_per_x:
                self.walls_per_x[x] = []
            self.walls_per_x[x].append(w)
            if y not in self.walls_per_y:
                self.walls_per_y[y] = []
            self.walls_per_y[y].append(w)
        if tr != 2:
            cm = self.display.module["play"]
            cm.add_wall(w)
        if tr == 1:
            w.properties.append("appearing")
            w.properties_t.append(1)

    def robot_move(self,robot):
        di = robot.status[1]
        robot.pos[0] += di[0]
        robot.pos[1] += di[1]

        shifts = [[0.5,0,0,1],
                 [0,0.5,1,0],
                 [0.5,1,0,-1],
                 [1,0.5,-1,0]
             ]
        shift = shifts[robot.status[2]]
        px = int(round(robot.pos[0]))+int(robot.size*shift[0])
        py = int(round(robot.pos[1]))+int(robot.size*shift[1])
        wx,wy = int(px/robot.size),int(py/robot.size)

        walls = []
        walls += self.walls_per_x.get(wx,[])
        walls += self.walls_per_y.get(wy,[])

        col = robot.check_collide(walls,px,py)
        if col:
            robot.place_next(col[0].pos,col[0].size,robot.size*shift[2],robot.size*shift[3])
            robot.create_col_effect()
            robot.status[0] = 0

        max_w = (self.walls[0].size*self.grid_size[0])
        max_h = (self.walls[0].size*self.grid_size[1])+robot.size
        if px < -50 or py < -50 or px > max_w+50 or py > max_h+50:
            #print("end of the level")
            self.display.play_sound("victory")
            cm = self.display.module["play"]
            tt = cm.get_time()
            le = self.loaded.get("length",-1)
            if le != -1:
                tt = ((le*10)-tt)+((le+2-cm.motion)*20)+le
            else:
                tt = 200-tt
            self.latest_score = int(tt)
            if not (cm.level_name.startswith("random") or cm.level_name.startswith("!")):
                self.profile.currentscore += int(tt)
                self.profile.add_score(cm.level_name,int(tt))
            if not cm.next_level.startswith("random") and not cm.next_level.startswith("!"):
                self.profile.lastlevel = cm.next_level
            self.profile.save()
            cm.level_name = cm.next_level
            cm.pause_mode = "pause"
            cm.remove_curtain()
            self.new_game()

        if abs(di[0])<10:
            di[0] *= 1.1
        if abs(di[1])<10:
            di[1] *= 1.1
        return wx,wy

    def run(self):
        while self.play:
            self.display.update()
            if self.display.mode != "pause":
                self.time += 1
                self.update()
        self.display.end()

    def update(self):
        pass


