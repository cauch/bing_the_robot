#! /usr/bin/env python3

#    This file is part of Bing_the_robot.
#
#    Bing_the_robot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Bing_the_robot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Bing_the_robot.  If not, see <http://www.gnu.org/licenses/>.

import random

class Generator:
    def __init__(self):
        self.wall = []
        self.free = []
        self.stop = []
        self.fake = []
        self.p_in = None
        self.p_out = None
        self.current = []
        self.size_w = 40
        self.size_h = 29
        self.number = random.randint(3,7)+random.randint(3,7)#+random.randint(3,7)
        self.d2v = [[1,0],[-1,0],[0,1],[0,-1]]
        self.d2n = [[2,3],[2,3],[0,1],[0,1]]
        self.d2o = [1,0,3,2]
        self.non_breaking = 0
        self.remove_useless_walls = 0
        self.stop_when_found = 1
        self.verbose = 0

    def set_seed(self,seed):
        random.seed(seed)

    def set_number(self,mi,ma):
        self.number = 0
        for i in range(3):
            self.number += random.randint(mi,ma)

    def set_size(self,w,h):
        self.size_w = w
        self.size_h = h
        
    def find_reasonnable_distance(self,initial,dire,wd=0):
        d = random.randint(1,max(self.size_w,self.size_h))
        p = initial[:]
        v = self.d2v[dire]
        fail = 0
        for i in range(d):
            p[0],p[1] = p[0]+v[0],p[1]+v[1]
            if p in self.wall:
                d = i+1
                break
            if p in self.stop:
                fail = 1
                break
        if d <= 1:
            fail = 1
        if fail and wd<10:
            return self.find_reasonnable_distance(initial,dire,wd+1)
        if fail:
            return None
        return d

    def new_line(self,p_in,dire):
        d = self.find_reasonnable_distance(p_in,dire)
        if d == None:
            return None,None
        p_out = self.fill(p_in,dire,d)
        if p_out == None:
            return None,None
        dire = self.d2n[dire][random.randint(0,1)]
        return p_out,dire


    def fill(self,initial,dire,d,fake=0):
        p = initial[:]
        v = self.d2v[dire]
        nf = []
        for i in range(d):
            p[0],p[1] = p[0]+v[0],p[1]+v[1]
            nf.append(p[:])
        pw = [p[0]+v[0],p[1]+v[1]]
        if pw in self.free:
            return None
        if fake==1:
            if p in self.free:
                return None
        if pw not in self.wall:
            self.wall.append(pw)
        self.stop.append(p)
        self.free += nf
        if fake:
            self.fake += nf
        return p

    def find_outside(self,p,d,m):
        dire = [1,0,3,2][d]
        fail = 0
        pp = p[:]
        v = self.d2v[dire]
        while 1:
            pp[0],pp[1] = pp[0]+v[0],pp[1]+v[1]
            if pp in self.wall:
                fail = 1
                break
            if pp[0] < m[0]:
                break
            if pp[0] > m[1]:
                break
            if pp[1] < m[2]:
                break
            if pp[1] > m[3]:
                break
        if fail:
            return None
        self.stop.append(pp[:])
        pp3 = [pp[0]+v[0],pp[1]+v[1]]
        self.wall.append(pp3)
        pp2 = p[:]
        while 1:
            pp2[0],pp2[1] = pp2[0]+v[0],pp2[1]+v[1]
            if pp2 in self.wall:
                break
            self.free.append(pp2[:])
        self.wall.remove(pp3)
        #if p in self.stop:
        #    self.stop.remove(p)
        return pp

    def generate_try(self):
        self.p_in,dire = [0,0],random.randint(0,3)
        self.p_in_dire = dire
        self.stop.append(self.p_in)

        #1) create the non-squeezed non-shifted map
        if self.verbose:
            print("1) create the initial map")
        self.free.append(self.p_in)
        p_in = self.p_in
        for i in range(self.number):
            p_in,dire = self.new_line(p_in,dire)
            if p_in == None:
                return 0

        self.p_out = p_in
        self.p_out_dire = dire
        if abs(self.p_out[0]-self.p_in[0])+abs(self.p_out[1]-self.p_in[1]) < 3:
            return 0

        #2) add entrance and exit at the borders
        if self.verbose:
            print("2) get border outside")
        ox = [x[0] for x in self.stop]
        oy = [x[1] for x in self.stop]
        x_min,x_max = min(ox),max(ox)
        y_min,y_max = min(oy),max(oy)

        #extend the border a bit
        x_min += -1*random.randint(3,8)
        y_min += -1*random.randint(3,8)
        x_max += random.randint(3,8)
        y_max += random.randint(3,8)

        self.p_in = self.find_outside(self.p_in,self.p_in_dire,[x_min,x_max,y_min,y_max])
        if self.p_in == None:
            return 0
        self.p_out = self.find_outside(self.p_out,self.p_out_dire,[x_min,x_max,y_min,y_max])
        if self.p_out == None:
            return 0

        #3) look at the dimension before squeezing and shifting
        if self.verbose:
            print("3) can we squeeze it in")
        ox = list(set([x[0] for x in self.stop]+[x[0] for x in self.wall]))
        oy = list(set([x[1] for x in self.stop]+[x[1] for x in self.wall]))

        #if there is already too much intermediate points, it's already useless
        if len(ox) > self.size_w-2:
            return 0
        if len(oy) > self.size_h-2:
            return 0

        #4) squeeze
        if self.verbose:
            print("4) squeeze")
        self.squeeze(0,ox,self.size_w-2)
        self.squeeze(1,oy,self.size_h-2)
        nf = []
        for ff in self.free:
            if ff not in nf:
                nf.append(ff)
        self.free = nf

        #5) shift
        if self.verbose:
            print("5) shift")

        #find anchors
        anchors = [None,None,None,None]
        anchors[self.p_in_dire] = self.p_in
        anchors[self.p_out_dire] = self.p_out
        if anchors[0] == None and anchors[1] == None:
            anchors[0] = [min([x[0]-1 for x in self.wall]),0]
        if anchors[2] == None and anchors[3] == None:
            anchors[2] = [0,min([x[1]-1 for x in self.wall])]

        #move
        if anchors[0] != None:
            self.shift(0,anchors[0][0]*-1)
        elif anchors[1] != None:
            self.shift(0,self.size_w-anchors[1][0]-1)
        if anchors[2] != None:
            self.shift(1,anchors[2][1]*-1)
        elif anchors[3] != None:
            self.shift(1,self.size_h-anchors[3][1]-1)

        #6) add contour
        if self.verbose:
            print("6) add contours")
        list_ = []
        list_ += [ [0,x] for x in range(self.size_h) ]
        list_ += [ [x,0] for x in range(1,self.size_w-1) ]
        list_ += [ [self.size_w-1,x] for x in range(self.size_h) ]
        list_ += [ [x,self.size_h-1] for x in range(1,self.size_w-1) ]
        for l in list_:
            if l == self.p_in:
                continue
            if l == self.p_out:
                continue
            if l in self.wall:
                continue
            self.wall.append(l)


        if self.non_breaking:
            if self.verbose:
                print("compute the solution")
            current_solution = self.solve(self.wall,self.p_in,self.p_out)
            self.current_l = len(current_solution)

        #7) add red herring paths
        if self.verbose:
            print("7) red herring")
        if self.p_in in self.stop:
            self.stop.remove(self.p_in)
        if self.p_out in self.stop:
            self.stop.remove(self.p_out)

        i = 0
        while i < 100+(self.number*10):
            i += self.add_redherring()

        #8) add bloc to complicate backtracing
        if self.verbose:
            print("8) anti-backtracking")
        i = 0
        self.aback = []
        while i < 100+(self.number*10):
            i += self.add_antiback()        

        return 1

    def add_antiback(self):

        #to be redone

        p = self.stop[random.randint(0,len(self.stop)-1)]
        if p in self.aback:
            return 1
        p0 = [p[0]+1,p[1]+0,1,2,3]
        p1 = [p[0]-1,p[1]+0,0,2,3]
        p2 = [p[0]+0,p[1]+1,3,0,1]
        p3 = [p[0]+0,p[1]-1,2,0,1]
        pp = []
        for pi in [p0,p1,p2,p3]:
            if pi[:2] in self.wall:
                pp.append(pi)
        if len(pp) != 1:
            return 1
        dire = pp[0][2]
        dire_side = pp[0][3:]
        random.shuffle(dire_side)

        v = self.d2v[dire]
        #find how far we can go
        d_max = -1
        pp2 = p[:]
        while 1 and d_max < 105:            
            d_max += 1
            pp2[0],pp2[1] = pp2[0]+v[0],pp2[1]+v[1]
            if pp2 in self.wall:
                break        
            if pp2 == self.p_in or pp2 == self.p_out:
                break
        if d_max <=2 or d_max > 100:
            return 1

        d_ch = random.randint(2,d_max)
        pp2 = p[:]
        for i in range(d_ch):
            pp2[0],pp2[1] = pp2[0]+v[0],pp2[1]+v[1]
        v_side = self.d2v[dire_side[0]]
        v_oside = self.d2v[dire_side[1]]
        pp3 = [pp2[0]+v_side[0],pp2[1]+v_side[1]]
        if pp3 in self.free+self.wall+self.stop:
            return 1
        pp4 = pp2[:]
        found = 0
        for i in range(30):
            pp4[0],pp4[1] = pp4[0]+v_oside[0],pp4[1]+v_oside[1]
            if pp4 in self.wall:
                break
            vs = self.d2v[self.d2n[dire_side[0]][0]]
            pp4u = [pp4[0]+vs[0],pp4[1]+vs[1]]
            vs = self.d2v[self.d2n[dire_side[0]][1]]
            pp4d = [pp4[0]+vs[0],pp4[1]+vs[1]]
            if pp4u in self.wall:
                found = 1
                break
            if pp4d in self.wall:
                found = 1
                break

        if found or random.randint(0,10)==0:
            #r = self.fill(pp2,self.d2o[dire],d_ch,2)

            if self.non_breaking:
                new_solution = self.solve(self.wall+[pp3],self.p_in,self.p_out)
                if len(new_solution) < self.current_l:
                    return 1


            self.wall.append(pp3)
            self.stop.append(pp2)
            self.fake.append(pp3)
            self.aback.append(p)
            return 10
        return 1

    def add_redherring(self):
        p = self.stop[random.randint(0,len(self.stop)-1)]
        p0 = [p[0]+1,p[1]+0,0]
        p1 = [p[0]-1,p[1]+0,1]
        p2 = [p[0]+0,p[1]+1,2]
        p3 = [p[0]+0,p[1]-1,3]
        pp = []
        nw,nf = 0,0
        for pi in [p0,p1,p2,p3]:
            if pi[:2] not in self.wall+self.free:
                pp.append(pi)
            if pi[:2] in self.wall:
                nw += 1
            if pi[:2] in self.free:
                nf += 1
        if len(pp) != 1 or nw != 1 or nf != 2:
            return 1

        dire = pp[0][2]
        v = self.d2v[dire]
        #find how far we can go
        d_max = -1
        pp2 = p[:]
        while 1:
            d_max += 1
            pp2[0],pp2[1] = pp2[0]+v[0],pp2[1]+v[1]
            if pp2 in self.wall:
                break
            if d_max > max(self.size_w,self.size_h):
                return 1

        if d_max <=2:
            return 1

        d_ch = random.randint(2,d_max)

        np = [p[0]+(d_ch*self.d2v[dire][0]),p[1]+(d_ch*self.d2v[dire][1])]
        if self.non_breaking:
            new_solution = self.solve(self.wall+[np],self.p_in,self.p_out)
            if len(new_solution) < self.current_l:
                return 1

        np = self.fill(p,dire,d_ch,1)
        if np == None:
            return 1
        return 10

    def shift(self,ind,sh):
        v = [0,0]
        v[ind] = sh
        self.wall = [[x[0]+v[0],x[1]+v[1]] for x in self.wall]
        self.free = [[x[0]+v[0],x[1]+v[1]] for x in self.free]
        self.stop = [[x[0]+v[0],x[1]+v[1]] for x in self.stop]
        self.p_in = [self.p_in[0]+v[0],self.p_in[1]+v[1]]
        self.p_out = [self.p_out[0]+v[0],self.p_out[1]+v[1]]

    def squeeze(self,ind,li,di):
        vmin = min(li)
        vmax = max(li)
        add = random.randint(1,2)+random.randint(1,2)
        if self.p_in_dire == 0+(2*ind) and self.p_out_dire == 1+(2*ind): add = -1
        if self.p_in_dire == 1+(2*ind) and self.p_out_dire == 0+(2*ind): add = -1
        for ii in range(max(0,vmax-vmin-di+add)):
            vmin = min(li)
            vmax = max(li)
            vtr = random.randint(vmin,vmax)
            wd = 0
            while vtr in li:
                wd += 1
                if wd > 1000:
                    break
                vtr = random.randint(vmin,vmax)
            li = [y[ind] for y in self.schrink_list(vtr,ind,[[x,x] for x in li])]
            self.wall = self.schrink_list(vtr,ind,self.wall)
            self.free = self.schrink_list(vtr,ind,self.free)
            self.stop = self.schrink_list(vtr,ind,self.stop)
            self.p_in = self.schrink_list(vtr,ind,[self.p_in])[0]
            self.p_out = self.schrink_list(vtr,ind,[self.p_out])[0]

    def schrink_list(self,vtr,ind,li):
        n_li = []
        for x in li:
            if x[ind] < vtr:
                n_li.append(x)
            elif x[ind] > vtr:
                n = x[:]
                n[ind] += -1
                n_li.append(n)
        return n_li

    def generate(self):
        if self.verbose:
            print("start generating")
        s = 0
        for i in range(1000):
            self.wall = []
            self.free = []
            self.stop = []
            self.fake = []
            s = self.generate_try()
            if s:
                break
            elif self.verbose:
                print("generation failed")
        return s

    def solve(self,walls,p_in,p_out):
        self.s_walls = walls[:]
        self.s_p_in = p_in
        self.s_p_out = p_out
        self.s_visited = [p_in]
        self.s_wall_touched = []
        self.s_walkers = []
        self.s_length = 0

        p0 = p_in[:]

        self.s_borders = [min([x[0] for x in self.s_walls]),
                          max([x[0] for x in self.s_walls]),
                          min([x[1] for x in self.s_walls]),
                          max([x[1] for x in self.s_walls])]
        borders = self.s_borders

        histo = []

        for i in [0,1,2,3]:
            self.s_walkers.append([p0,i,[p0],0])

        found = []
        add_wall_in_p_in = 0
        while self.s_walkers:
            for w in self.s_walkers[:]:
                i = self.walk(w)
                if i != 0 and found == []:
                    found = i
                    if self.verbose:
                        print("found a solution",len(self.s_walkers))
                    if self.stop_when_found == 1:
                        self.s_walkers = []
                        break
            self.s_walkers = [w for w in self.s_walkers if w[1] != -1]
            self.s_walkers = [w for w in self.s_walkers if w[0][0] >= borders[0]]
            self.s_walkers = [w for w in self.s_walkers if w[0][0] <= borders[1]]
            self.s_walkers = [w for w in self.s_walkers if w[0][1] >= borders[2]]
            self.s_walkers = [w for w in self.s_walkers if w[0][1] <= borders[3]]
            #if self.verbose:
            #    print("number of walkers",len(self.s_walkers),len(self.s_visited))
            #    if len(self.s_walkers): print(self.s_walkers[-1])
            if add_wall_in_p_in == 0:
                add_wall_in_p_in = 1
                self.s_walls.append(p_in)
        return found

    def walk(self,w):
        if w[1] == -1:
            return 0

        borders = self.s_borders

        v = self.d2v[w[1]]
        w[1] = -1
        w[3] += 1
        pp = w[0][:]
        found = 0
        histo = []
        while 1:
            pp[0],pp[1] = pp[0]+v[0],pp[1]+v[1]
            histo.append(pp[:])
            if pp == self.s_p_out:
                found = 1
                break
            if pp[0] < borders[0] or pp[0] > borders[1]:
                break
            if pp[1] < borders[2] or pp[1] > borders[3]:
                break
            if pp in self.s_walls:
                if pp not in self.s_wall_touched:
                    self.s_wall_touched.append(pp[:])
                histo.remove(pp)
                pp[0],pp[1] = pp[0]-v[0],pp[1]-v[1]
                break
        if found:
            self.s_length = w[3]
            return w[2]+histo+[pp]
        if pp in self.s_visited:
            return 0
        nh = w[2]+histo
        if pp not in nh:
            nh.append(pp)
        self.s_visited.append(pp)
        for i in [0,1,2,3]:
            self.s_walkers.append([pp,i,nh,w[3]])
        return 0


if __name__ == '__main__':
    g = Generator()
    g.non_breaking = 1
    g.verbose = 1
    i = g.generate()
    if i != 0:
        print('self.loaded["walls"]=',g.wall)
        if g.p_in[0] == 0:
            print('self.loaded["dire"]=0')
        elif g.p_in[1] == 0:
            print('self.loaded["dire"]=2')
        elif g.p_in[0] == g.size_w-1:
            print('self.loaded["dire"]=1')
        elif g.p_in[1] == g.size_h-1:
            print('self.loaded["dire"]=3')
        print('self.loaded["stop"]=',g.stop)
        print('self.loaded["free"]=',g.free)
        print('self.loaded["fake"]=',g.fake)
        print('self.loaded["p_in"]=',g.p_in)
        print('self.loaded["p_out"]=',g.p_out)
        g.stop_when_found = 0
        r = g.solve(g.wall,g.p_in,g.p_out)
        print('self.loaded["solution"]=',r)
        print('self.loaded["length"]=',g.s_length)
        l = []
        for ll in g.s_wall_touched:
            if ll not in l and ll != g.p_in:
                l.append(ll)
        print('self.loaded["walls_u"]=',l)
