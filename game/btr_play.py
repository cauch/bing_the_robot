#! /usr/bin/env python3

#    This file is part of Bing_the_robot.
#
#    Bing_the_robot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Bing_the_robot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Bing_the_robot.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random
import math
import time

class Robot(pygame.sprite.Sprite):
    def __init__(self,core):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.play = core.display.module["play"]
        self.size = core.display.size
        self.image = pygame.Surface((self.size,self.size)).convert_alpha()
        self.rect = self.image.get_rect()
        self.nr = 0 #need refresh
        self.blink_tags = [1,0,0,1,1,1]
        self.pos = [0,0]
        self.status = [0,[0,0],0,-1]
        self.last_col = 0
        self.sound_to_play = [0,[]]
        self.refresh()

    def refresh(self):
        if self.status[0] == -100:
            self.status[0] = -99
            return 1
        if self.status[0] == -99:
            return 0
        need_image_refresh = 0
        need_image_refresh = self.blink()
        if self.sound_to_play[0] != 0:
            self.sound()
        if self.status[0] == 0 and self.nr == 0 and need_image_refresh == 0:
            return 0
        if self.status[0] != 0:
            self.move()
        self.nr = 0
        if self.last_col != 0:
            self.last_col += -1
        d = self.status[2]
        if d != self.status[3]:
            self.status[3] = d
            need_image_refresh = 1
        if need_image_refresh:
            self.image.fill((0,0,0,0))
            image1 = self.display.images["bing%i"%d]
            self.image.blit(image1,(0,0))
            for i in range(6):
                if self.blink_tags[i] > 0:
                    image2 = self.display.images["bing_pL%i%i"%(i+1,d)]
                    self.image.blit(image2,(0,0))
        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1

    def check_collide(self,walls,px,py):
        col = []
        for w in walls:
            if w.rect.collidepoint(px,py):
                col.append(w)
        return col

    def place_next(self,wp,ws,dx,dy):
        self.pos[0] = int(wp[0]*ws)+(dx)
        self.pos[1] = int(wp[1]*ws)+(dy)+self.play.screen_topbar

    def explode(self):
        if self.status[0] <= -99:
            return
        self.status[0] = -100
        self.display.play_sound("explo")
        vds = [[-1,1],[1,1],[-1,-1],[-1,1]]
        for j in range(8):
            ds = vds[j%4]
            v = Visual(self.core)
            dsm = [x*(0.3+(random.random()*0.7)) for x in ds]
            na = 50+(random.random()*50)
            v.status = [int(na),dsm,0]
            v.pos = [self.pos[0]+(self.size/2),self.pos[1]+(self.size/2)]
            v.what = "explo"
            self.play.layer2_obj.append(v)
        self.rect.left = -100

    def move(self):
        if self.play.pause_mode != "none":
            return
        wx,wy = self.core.robot_move(self)
        #if self.play.pause_mode == "none":
        #    i = (((wy*self.play.grid_width)+wx)*2)+1
        #    self.containers.change_layer(self,i)

    def blink(self):
        if self.play.pause_mode != "none":
            return 0
        nir = 0
        for i in range(6):
            if self.blink_tags[i] == 0:
                nir = 1
                self.blink_tags[i] = random.randint(10,100)
            elif self.blink_tags[i] == 1:
                nir = 1
                self.blink_tags[i] = -1*random.randint(10,500)
            if self.blink_tags[i] < 0:
                self.blink_tags[i] += 1
            else:
                self.blink_tags[i] += -1
        return nir

    def sound(self):
        self.sound_to_play[0] += -1
        if self.sound_to_play[0] == 0:
            ll = self.sound_to_play[1]
            self.display.play_sound(ll[0],1)
            for l in ll[1:]:
                self.display.play_sound(l,0.5)
                
    def create_col_effect(self):
        a = pow(self.status[1][0],2)+pow(self.status[1][1],2)
        na = min(50,(5*math.sqrt(a)))
        if a < 5.1:
            return
        #1) sounds
        self.display.play_sound(self.display.random_sound_from_list("impact"),na*0.005)
        #2) bit of dust
        d = self.status[2]
        ps = [ [[0,0],[0,self.size],[0,self.size],[self.size,self.size]][d],
               [[self.size,0],[0,0],[self.size,self.size],[self.size,0]][d]]
        ds = [[[-1,1],[1,1],[-1,-1],[-1,1]][d],
              [[1,1],[1,-1],[1,-1],[-1,-1]][d]]
        for i in range(2):
            v = Visual(self.core)
            dsm = [x*(0.3+(random.random()*0.7)) for x in ds[i]]
            v.status = [int(na),dsm,0]
            v.pos = [self.pos[0]+ps[i][0],self.pos[1]+ps[i][1]]
            v.what = "dust"
            self.play.layer2_obj.append(v)
        if self.last_col != 0:
            return
        self.last_col = random.randint(0,50)+random.randint(0,50)
        #3) swearing text
        v = Visual(self.core)
        ri = random.randint(0,len(self.play.texts)-1)
        text = self.play.texts[ri]
        v.status = [60,text]
        v.pos = [self.pos[0]+10,self.pos[1]-10]
        v.what = "text"
        self.play.layer2_obj.append(v)
        #4) swearing sound
        self.sound_to_play[0] = 10
        self.sound_to_play[1] = self.play.sound_words[ri]

class Enemy(pygame.sprite.Sprite):
    def __init__(self,core):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.play = core.display.module["play"]
        self.size = core.display.size
        self.image = pygame.Surface((self.size,self.size)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.image_name = "en_r"
        self.dire = 0
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.pos = [0,0]
        self.param = []
        self.refresh()

    def move_r(self):
        dx,dy = 0,0
        if self.dire == 0:
            dy = self.size-8
        if self.dire == 1:
            dx = self.size-8
        cx = int((self.pos[0]+dx)/self.size)
        cy = int((self.pos[1]+dy-self.play.screen_topbar)/self.size)
        if [cx,cy] == self.param[0]:
            self.param = self.param[1:]+[self.param[0]]
            if self.image_name.startswith("en_c") and (cy < 0 or cy > self.play.grid_height):
                self.image_name = "en_c%i"%random.randint(1,5)
        px,py = self.param[0]
        v,d = [0,-1],0
        if py < cy:
            v,d = [0,-1],0
        if py > cy:
            v,d = [0,1],2
        if px < cx:
            v,d = [-1,0],1
        if px > cx:
            v,d = [1,0],3
        self.dire = d
        vi = 8
        if self.image_name.startswith("en_c"):
            vi = 10
        self.pos[0] += v[0]*vi
        self.pos[1] += v[1]*vi
        if self.core.robot.rect.colliderect(self.rect):
            self.core.robot.explode()

    def move_d(self):
        dx = self.core.robot.pos[0]-self.pos[0]
        dy = self.core.robot.pos[1]-self.pos[1]
        r = math.sqrt(pow(dx,2)+pow(dy,2))
        if r == 0:
            dx,dy = 0,0
        else:
            dx = dx*1./r
            dy = dy*1./r
        d = 0
        if dy > 0 and max(abs(dy),abs(dx)) == abs(dy):
            d = 2
        if dx > 0 and max(abs(dx),abs(dy)) == abs(dx):
            d = 3
        if dy < 0 and max(abs(dy),abs(dx)) == abs(dy):
            d = 0
        if dx < 0 and max(abs(dy),abs(dx)) == abs(dx):
            d = 1
        self.dire = d
        self.pos[0] += dx*1
        self.pos[1] += dy*1
        if self.core.robot.rect.colliderect(self.rect):
            self.core.robot.explode()

    def move(self):
        if self.image_name == "en_r":
            self.move_r()
        elif self.image_name.startswith("en_c"):
            self.move_r()
        elif self.image_name == "en_d":
            self.move_d()

    def refresh(self):
        if self.nr == 0:
            return 0
        if self.play.pause_mode != "none":
            return 0

        self.nr = 1
        self.move()
        if self.image_name == "en_d":
            if len(self.param)==2:
                self.param[1] += -1
                if self.param[1] < 0:
                    self.param[1] = 400+random.randint(0,400)+random.randint(0,400)
                    self.pos = [self.param[0][0]*self.size,(self.param[0][1]*self.size)+self.play.screen_topbar]

        self.image = self.display.images[self.image_name+"%i"%self.dire]
        self.rect = self.image.get_rect()
        self.rect.left = int(round(self.pos[0]))
        self.rect.top = int(round(self.pos[1]))
        return 1


class Visual(pygame.sprite.Sprite):
    def __init__(self,core):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.play = core.display.module["play"]
        self.size = core.display.size
        self.image = pygame.Surface((10,10)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.rect = self.image.get_rect()
        self.nr = 1 #need refresh
        self.pos = [0,0]
        self.status = [10,[0,0]]
        self.ontop = 0
        self.what = "none"
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        if self.play.pause_mode != "none":
            return 0
        self.nr = 1
        self.status[0] += -1
        if self.status[0] < 0:
            self.image.fill((0,0,0,0))            
            return -1
        if self.what in ["dust","explo"]:
            dev = self.status[1]
            self.pos[0] += dev[0]
            self.pos[1] += dev[1]
            size = int(self.status[0]*0.1)
            if size != self.status[2] or size <= 0:
                if self.what == "dust":
                    cl = (158,149,128,min(255,self.status[0]*2))
                if self.what == "explo":
                    cl = (255,80+random.randint(0,80),0,min(255,self.status[0]*2))
                if size*2 > 10:
                    self.image = pygame.Surface((size*2,size*2)).convert_alpha()
                self.image.fill((0,0,0,0))
                pygame.draw.circle(self.image,cl,(size,size),size)
                self.status[2] = size
        if self.what == "text":
            if self.status[0] > 50:
                pass
            elif self.status[0] == 50:
                t = self.status[1]
                cl = self.play.robottext_cl
                self.image = self.display.fonts["8"].render( t, 1, cl)
                self.pos = [self.core.robot.pos[0]+10+(self.image.get_width()/2),
                            self.core.robot.pos[1]-10+(self.image.get_width()/2)]
            else:
                self.pos[0] += 0.05
                self.pos[1] += -0.05
                self.image.fill((255, 255, 255, 250), None, pygame.BLEND_RGBA_MULT)
        self.rect.left = int(round(self.pos[0]))-(self.image.get_width()/2)
        self.rect.top = int(round(self.pos[1]))-(self.image.get_width()/2)
        if self.ontop==0:
            self.ontop=1
            self.play.layer2_sprite.move_to_front(self)
        return 1


class Wall(pygame.sprite.Sprite):
    def __init__(self,core):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.play = core.display.module["play"]
        self.size = core.display.size
        self.image = pygame.Surface((self.size+5,self.size+5)).convert_alpha()
        self.image_name = self.play.image_bloc[random.randint(0,len(self.play.image_bloc)-1)]
        self.rect = self.image.get_rect()
        self.pos = [0,0]
        self.nr = 1
        self.properties = []
        self.properties_t = []
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        self.nr = 0

        transparency = 255
        if "appearing" in self.properties:
            a_i = self.properties.index("appearing")
            if self.play.pause_mode == "none":
                self.properties_t[a_i] += 1
            transparency = 255-int((100-self.properties_t[a_i])*2.55)
            if self.properties_t[a_i] == 15:
                self.core.add_wall(self.pos[0],self.pos[1],2,self)
                if [self.pos[0]-1,self.pos[1]] in self.core.walls_coord:
                    [x for x in self.core.walls_per_x[self.pos[0]-1] if x.pos[1] == self.pos[1]][0].nr=1
                if [self.pos[0],self.pos[1]-1] in self.core.walls_coord:
                    [x for x in self.core.walls_per_y[self.pos[1]-1] if x.pos[0] == self.pos[0]][0].nr=1
            if self.properties_t[a_i] >= 100:
                self.properties[a_i] = None
                transparency = 255
            self.nr = 1

        if "inv" in self.properties:
            a_i = self.properties.index("inv")
            if self.play.pause_mode == "none":
                self.properties_t[a_i] += -1            
            if self.properties_t[a_i] <= 0:
                self.properties_t[a_i] = 0
            else:
                self.nr = 1
            if self.properties_t[a_i]%5 != 0:
                return 0
            transparency = int(self.properties_t[a_i]*5)

        pos_x = int(round(self.pos[0]*self.size))
        if "blink" in self.properties:
            self.nr = 1
            a_i = self.properties.index("blink")
            if self.play.pause_mode == "none":
                self.properties_t[a_i] += -1
            if self.properties_t[a_i] > 1000:
                #first time
                self.properties_t[a_i] += -1000
            elif self.properties_t[a_i] == 0:
                #from visible to invisible
                pos_x = -100
            elif self.properties_t[a_i] == -100:
                #back to visible
                self.properties_t[a_i] = 100
            else:
                return 0

        self.rect.left = pos_x
        self.rect.top = int(round(self.pos[1]*self.size))+self.play.screen_topbar
        self.image.fill((0,0,0,0))

        image1 = self.display.images[self.image_name]
        self.image.blit(image1,(0,0))        
        #pygame.draw.rect(self.image,(0,150,10),pygame.Rect(0,0,self.size,self.size))
        if [self.pos[0]+1,self.pos[1]] not in self.core.walls_coord:
            pygame.draw.polygon(self.image,(100,100,100,100),[(self.size,0),(self.size+5,5),(self.size+5,self.size+4),(self.size,self.size-1)])
        else:
            pygame.draw.polygon(self.image,(100,100,100,100),[(self.size,0),(self.size+5,5),(self.size+5,self.size-1),(self.size,self.size-1)])
        if [self.pos[0],self.pos[1]+1] not in self.core.walls_coord:
            pygame.draw.polygon(self.image,(100,100,100,100),[(1,self.size),(6,self.size+5),(self.size+5,self.size+5),(self.size,self.size)])
        else:
            pygame.draw.polygon(self.image,(100,100,100,100),[(1,self.size),(6,self.size+5),(self.size-1,self.size+5),(self.size-1,self.size)])
        if transparency != 255:
            self.image.fill((255, 255, 255, transparency), None, pygame.BLEND_RGBA_MULT)
        return 1

class Topbar(pygame.sprite.Sprite):
    def __init__(self,core):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.play = core.display.module["play"]
        self.size = self.display.size
        self.image = pygame.Surface((self.display.screen_width,self.play.screen_topbar)).convert_alpha()
        self.image.fill((0,0,0))
        self.rect = self.image.get_rect()
        self.time_text = ["",None]
        self.header = ["!",None,None]
        self.current_score = [-1,None]
        self.first=1
        self.refresh()

    def refresh(self):
        tt = self.play.get_time()
        text = time.strftime("%H:%M:%S", time.gmtime(tt))
        if text != self.time_text[0]:
            image = self.display.fonts["12"].render( text, 1, (255,255,255))
            self.time_text[1] = image
            self.time_text[0] = text
        else:
            if self.first==1:
                self.first=0
            else:
                return 0

        if self.header[0] != self.core.loaded.get("levelname",""):
            self.header[0] = self.core.loaded.get("levelname","")
            self.header[1] = self.display.fonts["12"].render( self.core.loaded.get("chapter",""), 1, (255,255,255))
            self.header[2] = self.display.fonts["12"].render( self.core.loaded.get("level",""), 1, (255,255,255))

        if self.current_score[0] != self.core.profile.currentscore:
            self.current_score[0] = self.core.profile.currentscore
            self.current_score[1] = self.display.fonts["12"].render( "score: %i"%self.core.profile.currentscore, 1, (255,255,255))

        self.containers.move_to_front(self)
        self.image.fill((0,0,0))

        w = self.display.screen_width
        self.image.blit(self.header[1],(2,1))
        self.image.blit(self.header[2],(2+10+self.header[1].get_width(),1))
        wu = self.header[1].get_width()+self.header[2].get_width()
        wn = (w/2)
        if wn < wu:
            wn = wu+10
        self.image.blit(self.current_score[1],(wn,1))
        self.image.blit(self.time_text[1],(w-self.time_text[1].get_width()-5,1))        
        return 1

class Curtain(pygame.sprite.Sprite):
    def __init__(self,core):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.play = core.display.module["play"]
        self.size = self.display.size
        self.image = pygame.Surface((self.display.screen_width,self.display.screen_height-self.play.screen_topbar)).convert_alpha()
        self.image.fill((0,0,0))
        self.rect = self.image.get_rect()
        self.rect.left = 0
        self.rect.top = self.play.screen_topbar
        self.text = ""
        self.nr = 2
        self.refresh()

    def refresh(self):
        if self.nr == 0:
            return 0
        if self.nr == 2:
            self.nr = 1
        else:
            self.nr = 0
        self.play.layer1_sprite.move_to_front(self)
        self.image.fill((0,0,0))
        cl = (255,255,255)
        y = 50
        for t in self.text.split("\n"):
            f = "18"
            if t.startswith("!S"):
                f = "12"
                t = t[2:]
            image1 = self.display.fonts[f].render( t, 1, cl)
            self.image.blit(image1,(50,y))
            y += 30
        return 1

class BtrPlay:
    def __init__(self,display):
        self.display = display
        self.core = display.core
        self.size = display.size
        self.screen_topbar = 20

        self.robottext_cl = (230,100,0)

        self.pause_mode = "curtain"

        self.time = [None,0]
        self.motion = 0

        self.level_name = "ch01_cs"
        if self.core.profile.lastlevel:
            self.level_name = self.core.profile.lastlevel
        self.next_level = "random"

        self.wait = 20

        self.grid_width = 40
        if self.grid_width*self.display.size != self.display.screen_width:
            print("Warning: grid width not equal to screen")
        self.grid_height = 29
        if self.screen_topbar+(self.grid_height*self.display.size) != self.display.screen_height:
            print("Warning: grid height not equal to screen")

        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn

        self.image_tile = ["tile_dirt011","tile_dirt012"]
        self.image_bloc = ["bloc_wip"]
        
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None

        self.texts = ["ouch!","ow!","damnit!","aïe!","arf!","damn!","oh no!",
                      "ow, that hurts!","my head!","my sensors!","not again!",
                      "what?","did not seen that","excuse me sir","oops!",
                      "right in the kisser","ahhh, what a day","what a job!",
                      "and there we go again","who put that there!","who am I?",
                      "hey, it was not there last time!","where am I?",
                      "next time maybe?","I'm having a headache",
                      "one day I will pass","#!?$*","$?!@#$!","*£!%$!!",
                      "where are my glasses?","I love the tunnel effect"
                  ]
        self.sound_words = []

    def init(self):
        if self.sound_words == []:
            for i in range(len(self.texts)):
                self.sound_words.append( [self.display.random_sound_from_list("robot") for x in range(random.randint(4,8))] )


        self.layer1_sprite = pygame.sprite.LayeredUpdates()
        self.layer2_sprite = pygame.sprite.LayeredUpdates()
        Wall.containers = self.layer1_sprite
        Curtain.containers = self.layer1_sprite
        Topbar.containers = self.layer2_sprite
        Robot.containers = self.layer1_sprite
        Enemy.containers = self.layer2_sprite
        Visual.containers = self.layer2_sprite

        self.layer2_obj.append(Topbar(self.core))

    def set_background(self):
        if self.bkgd:
            return
        bg = []
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        for i in self.image_tile:
            bg.append(self.display.images[i])
        dy = self.screen_topbar
        pygame.draw.rect(self.bkgd,(0,0,0),pygame.Rect(0,0,rw,dy))

        #special for the last level
        #bkgd_right is the tile for the right
        bg2 = []
        if self.core.loaded.get("bkgd_right",[]):
            bg2 = [self.display.images[x] for x in self.core.loaded.get("bkgd_right",[])]

        maxx = int(rw/bg[0].get_width())
        for x in range(maxx):
            for y in range(int(rh/bg[0].get_height())):
                cbg = bg[random.randint(0,len(bg)-1)]

                if bg2:
                    thr = (maxx-x-6)*0.04
                    if random.random() > thr:
                        cbg = bg2[random.randint(0,len(bg2)-1)]                    
                self.bkgd.blit(cbg, (x*cbg.get_width(),dy+ (y*cbg.get_height()) ))

        for l in self.core.loaded.get("bkgd_rect",[]):
            pygame.draw.rect(self.bkgd,l[0],pygame.Rect(l[1],l[2],l[3],l[4]))

    def add_wall(self,w):
        self.layer1_obj.append(w)
        w.nr = 1
        i = ((w.pos[1]*self.grid_width)+w.pos[0])*2
        self.layer1_sprite.change_layer(w,i)

    def move_robot(self,d):
        if self.core.robot.status[0] != 0:
            return
        self.motion += 1
        self.core.robot.status[0] = 1
        self.core.robot.status[1] = d
        if d[0] < 0:
            self.core.robot.status[2] = 1
        if d[0] > 0:
            self.core.robot.status[2] = 3
        if d[1] < 0:
            self.core.robot.status[2] = 0
        if d[1] > 0:
            self.core.robot.status[2] = 2

    def check_key(self,key):
        if self.pause_mode == "none":
            if key == K_UP or key == K_z or key == K_w:
                self.move_robot([0,-2])
            elif key == K_DOWN or key == K_s:
                self.move_robot([0,2])
            elif key == K_LEFT or key == K_a or key == K_q:
                self.move_robot([-2,0])
            elif key == K_RIGHT or key == K_d:
                self.move_robot([2,0])

    def check_events(self,event):
        if event.type == ACTIVEEVENT:
            ##turn on pause if the window is losing focus
            #if self.core.config.pausewhenout:
            #    if event.state == 2 and event.gain == 0:
            #        self.mode = "pause"
            pass
        elif event.type == KEYDOWN:
            key = event.key
            if self.pause_mode == "curtain":
                self.remove_curtain()
                self.pause_mode = "none"
            elif self.pause_mode == "none":
                if key == K_F8:
                    self.core.robot.create_col_effect()
                elif key == K_ESCAPE or key == K_p:
                    self.set_pause()
                elif key == K_r:
                    self.reload_level()
            elif self.pause_mode == "pause":
                if key == K_p:
                    self.pause_mode = "none"
                    self.remove_curtain()
                if key == K_ESCAPE:
                    if self.motion >= 5:
                        self.core.profile.currentscore += -20
                        self.core.profile.save()
                    self.pause_mode = "none"
                    self.remove_curtain()
                    self.display.mode = "menu"
                    self.core.back_to_menu()

    def reload_level(self):
        if self.level_name.startswith("random"):
            self.core.robot.status = [0,[0,0],0,-1]
            dire = self.core.loaded.get("dire",0)
            p_in = self.core.loaded.get("p_in",[0,0])
            self.core.robot.place_next(p_in,self.core.robot.size,0,0)
            self.core.robot.nr = 1
            self.move_robot([[5,0],[-5,0],[0,5],[0,-5]][dire])
        else:
            self.core.profile.currentscore += -20
            self.core.profile.save()
            self.core.new_game()        

    def add_curtain(self,text):
        c = Curtain(self.core)
        c.text = text
        self.layer1_obj.append(c)
        self.curtain_obj = c

    def remove_curtain(self):
        self.curtain_obj.nr = 0
        if self.curtain_obj in self.layer1_obj:
            self.layer1_obj.remove(self.curtain_obj)
        self.curtain_obj.kill()

    def set_pause(self):
        ct = self.time[0]
        st = self.time[1]
        dt = time.time()-ct
        self.time[0] = None
        self.time[1] = st+dt
        self.pause_mode = "pause"
        t = "PAUSE\n\n"
        t += "press 'p' to continue\n"
        t += "press 'ESC' to abandon the game and exit to menu"
        #t += "\n\n"+self.core.loaded.get("intro","")
        self.add_curtain(t)


    def update(self):
        dirty = []

        if self.pause_mode == "none":
            if self.time[0] == None:
                self.time[0] = time.time()

        if self.layer1_sprite == None:
            return [],0

        if self.bkgd == None:
            self.set_background()
            self.display.screen.blit(self.bkgd, (0,0))

        redraw_s2 = 0
        redraw_s1 = 0
        tokill = []

        for o in self.layer2_obj[:]:
            r = o.refresh()
            if r==1:
                redraw_s2 = 1
            if r==-1:
                redraw_s2 = 1
                tokill.append(o)
                self.layer2_obj.remove(o)
        for o in self.layer1_obj[:]:
            r = o.refresh()
            if r==1:
                redraw_s1 = 1
            if r==-1:
                tokill.append(o)
                self.layer1_obj.remove(o)
        
        if redraw_s2 and self.layer2_sprite:
            self.layer2_sprite.clear(self.display.screen, self.bkgd)
            redraw_s1 = 1
        if redraw_s1 and self.layer1_sprite:
            self.layer1_sprite.clear(self.display.screen, self.bkgd)
            dirty.append(self.layer1_sprite.draw(self.display.screen))
        if redraw_s2 and self.layer2_sprite:
            dirty.append(self.layer2_sprite.draw(self.display.screen))

        for o in tokill:
            o.kill()

        if self.wait > 0:
            self.wait += -1
            return [],1

        redraw = 0
        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def get_time(self):
        dt = 0
        if self.time[0] != None:
            dt = time.time()-self.time[0]
        tt = dt+self.time[1]
        return tt

    def end(self):
        self.pause_mode = "curtain"
        self.wait = 20

        for o in self.layer1_obj:
            o.kill()
        for o in self.layer2_obj:
            o.kill()

        self.bkgd = None
        self.bkgd_first = 0
        s = ["1","2","3","4"][random.randint(0,3)]
        self.image_tile = ["tile_basic%sa"%s,"tile_basic%sb"%s,"tile_basic%sc"%s]
        self.image_bloc = ["bloc_basic1","bloc_basic2","bloc_basic3","bloc_basic4"]
        random.shuffle(self.image_bloc)
        self.image_bloc = self.image_bloc[:2]

        self.time = [None,0]

        self.motion = 0

        self.robottext_cl = (230,100,0)
        
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None
        
