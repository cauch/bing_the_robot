#! /usr/bin/env python3

#    This file is part of Bing_the_robot.
#
#    Bing_the_robot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Bing_the_robot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Bing_the_robot.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random

from game.btr_play import Wall,Robot

class Menuitem(pygame.sprite.Sprite):
    def __init__(self,menu,options):
        """
        Sprite corresponding to an elemet of the menu
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.menu = menu
        self.options = options
        self.isontop = 0
        if options.get("?") == "bkgd":
            #background of the panel
            xb = options.get("xb",0)
            dh = menu.display.screen_height
            dw = menu.display.screen_width
            height = options.get("h",dw-120)
            y = options.get("y",(dh-height)/2)
            self.image = pygame.Surface((dw-120-xb-xb,height)).convert_alpha()
            self.rect = self.image.get_rect()
            self.rect.left = 60+xb
            self.rect.top = y
            self.image.fill((255, 255, 255, 100))
        if options.get("?") == "text":
            self.cl = options.get("cl",(0,0,0))
            self.text = options.get("text","")
            f = options.get("f","18")
            self.image = menu.display.fonts[f].render( self.text, 1, self.cl)
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
        if options.get("?") == "button":
            #button
            #added to self.menu.buttons
            self.cl = options.get("cl",(0,0,0))
            e = options.get("e",1)
            self.text = options.get("text","")
            f = options.get("f","18")
            text = menu.display.fonts[f].render( self.text, 1, self.cl)
            w = options.get("s",100)
            self.image = pygame.Surface((w+1,25+1)).convert_alpha()
            self.image.fill((0,0,0,0))
            pygame.draw.line(self.image,self.cl,(0,0),(w,0))
            pygame.draw.line(self.image,self.cl,(0,25),(w,25))
            pygame.draw.line(self.image,self.cl,(0,0),(0,25))
            pygame.draw.line(self.image,self.cl,(w,0),(w,25))
            dx = (w-text.get_width())/2
            #dx = 30
            self.image.blit(text, (dx,1))
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
            if e:
                self.menu.buttons.append(self)
            self.value = options.get("value",self.text)
        if options.get("?") == "cursor":
            #cursor (black square)
            self.image = pygame.Surface((10,10)).convert_alpha()
            self.rect = self.image.get_rect()
            self.rect.left = -100
            self.rect.top = -100
            self.image.fill((0, 0, 0))
            self.value = -1

    def refresh(self):
        if self.isontop == 0:
            self.isontop = 1
            self.menu.menu_sprite.move_to_front(self)
            return 1
        return 0

class BtrMenu:
    def __init__(self,display):
        self.display = display
        self.core = display.core
        self.size = display.size

        self.bkgd = None
        self.bkgd_first = 0

        self.position = "main"

        self.menu_sprite = None
        self.menu_obj = []
        self.robot_needtomove = 1

        self.buttons = []
        self.cursor = None

    def init(self):

        self.menu_sprite = pygame.sprite.LayeredUpdates()
        Menuitem.containers = self.menu_sprite
        Wall.containers = self.menu_sprite
        Robot.containers = self.menu_sprite

        self.set_background()
        self.display.screen.blit(self.bkgd, (0,0))

        self.my_walls()

        self.display_main()

    def my_walls(self):
        self.walls = [[4, 5], [5, 24], [13, 23], [12, 20], [5, 4], [3, 6], [3, 23], [4, 24], [12, 24], [22, 21], [11, 21], [20, 19], [21, 18], [20, 23], [21, 24], [23, 24], [23, 18], [32, 20], [33, 19], [33, 21], [33, 23], [32, 24], [31, 10], [32, 9], [36, 10], [35, 13], [37, 12], [36, 13], [34, 12], [38, 11], [31, 11], [37, 4], [35, 4], [38, 5],[38,6]]

        for i in self.walls:
            if i[1] < 16:
                i[1] += -3
            if i[1] > 16:
                i[1] += 3

        self.core.walls_coord = self.walls[:]
        self.core.grid_size = [40,29]
        self.grid_width = 40
        self.grid_height = 29
        self.wo = []
        for l in self.walls:
            w = Wall(self.core)
            w.pos = l
            self.core.add_wall(0,0,2,w)
            w.image_name = ["bloc_plain1","bloc_plain2"][random.randint(0,1)]
            self.menu_obj.append(w)
            w.nr = 1
            i = ((w.pos[1]*self.display.module["play"].grid_width)+w.pos[0])*2
            self.menu_sprite.change_layer(w,i)
            self.wo.append(w)

        
        self.robot = Robot(self.core)
        self.robot.check_collide = self.robot_check_collide
        self.robot.create_col_effect = self.robot_cce
        self.pause_mode = "none"
        self.screen_topbar = 20
        self.robot.play = self
        self.robot.place_next([4,4],self.robot.size,0,0)
        self.menu_obj.append(self.robot)
        self.robot.nr = 1
        dire = 2
        self.move_robot([[5,0],[-5,0],[0,5],[0,-5]][dire])

    def robot_cce(self):
        self.robot_needtomove = 1

    def robot_check_collide(self,walls,px,py):
        col = []
        cx,cy = int(px/self.robot.size),int(py/self.robot.size)-1
        if [cx,cy] in self.walls:
            i = self.walls.index([cx,cy])
            col = [self.wo[i]]
        return col

    def move_robot(self,d):
        if self.robot.status[0] != 0:
            return
        self.robot.status[0] = 1
        self.robot.status[1] = d
        if d[0] < 0:
            self.robot.status[2] = 1
        if d[0] > 0:
            self.robot.status[2] = 3
        if d[1] < 0:
            self.robot.status[2] = 0
        if d[1] > 0:
            self.robot.status[2] = 2
        self.robot_needtomove = 0

    def set_background(self):
        if self.bkgd:
            return
        
        lob = ["tile_ground01a%i"%x for x in range(4)]
        lob +=["tile_ground02a%i"%x for x in range(4)]
        bg = [self.display.images[l] for l in lob]
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        for x in range(int(rw/bg[0].get_width())):
            for y in range(int(rh/bg[0].get_height())):
                cbg = bg[random.randint(0,len(bg)-1)]
                self.bkgd.blit(cbg, (x*cbg.get_width(),y*cbg.get_height() ))

    def check_key(self,key):
        pass

    def check_events(self,event):
        if event.type == ACTIVEEVENT:
            pass
        elif event.type == KEYDOWN:
            key = event.key
            if key == K_UP or key == K_LEFT or key == K_z or key == K_w:
                self.move_cursor(-1)
            elif key == K_DOWN or key == K_RIGHT or key == K_s:
                self.move_cursor(1)
            elif event.key == K_SPACE or event.key == K_RETURN:
                    if self.cursor and self.cursor.value != -1:
                        self.click_on_button(self.cursor.value)

    def click_on_button(self,i,silent=0):
        if silent == 0:
            self.display.play_sound("button1",0.5)
        value = self.buttons[i].value
        if value == "quit":
            self.display.core.play = 0
        elif value == "play":
            self.display.mode = "play"
            cm = self.display.module["play"]
            cm.level_name = "ch01_cs"
            if self.core.profile.lastlevel:
                cm.level_name = self.core.profile.lastlevel
            self.core.new_game()
        elif value == "practice":
            self.display.mode = "play"
            cm = self.display.module["play"]
            cm.level_name = "random"
            self.core.new_game()
        elif value == "about":
            self.display_clean()
            self.display_about()
        elif value == "scores":
            self.display_clean()
            self.display_scores()
        elif value == "help":
            self.display_clean()
            self.display_help()
        elif value == "profile":
            self.display_clean()
            self.display_profile()
        elif value.startswith("proB_"):
            self.display_clean()
            self.display_profile(int(value.split("_")[1]))
        elif value.startswith("scoB_"):
            self.display_clean()
            self.display_scores(int(value.split("_")[1]))
        elif value.startswith("pro_"):
            self.core.profile.change_profile(value.split("_")[1])
            self.display_clean()
            self.display_profile()
        elif value == "proN":
            self.core.profile.create_new()
            self.display_clean()
            self.display_profile()
        elif value == "options":
            self.display_clean()
            self.display_options()
        elif value.startswith("optB_"):
            self.display_clean()
            self.display_options(value.split("_")[1])
        elif value == "back":
            self.display_clean()
            self.display_main()
        else:
            print("TODO",value)



    def update(self):
        dirty = []
        if self.robot_needtomove:
            d = [[5,0],[-5,0],[0,5],[0,-5]][random.randint(0,3)]
            self.move_robot(d)

        redraw_me = 0
        for o in self.menu_obj:
            r = o.refresh()
            if r:
                redraw_me = 1
        if redraw_me and self.menu_sprite:
            self.menu_sprite.clear(self.display.screen, self.bkgd)
            dirty.append(self.menu_sprite.draw(self.display.screen))

        redraw = 0
        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def set_cursor(self,i,silent=0):
        if self.cursor.value == i:
            #already in place
            return
        self.cursor.value = i
        if i >= len(self.buttons):
            #no button for this index
            self.cursor.rect.left = -100
            return
        if silent == 0:
            self.display.play_sound("button2",0.5)
        #place the sprite

        self.cursor.image = pygame.Surface((self.buttons[i].rect.width,self.buttons[i].rect.height)).convert_alpha()
        self.cursor.rect = self.cursor.image.get_rect()
        self.cursor.rect.left = self.buttons[i].rect.left
        self.cursor.rect.top = self.buttons[i].rect.top
        self.cursor.image.fill((0, 155, 255, 70))
        #self.cursor.rect.left = self.buttons[i].rect.left+6
        #self.cursor.rect.top = self.buttons[i].rect.top+(self.buttons[i].rect.height/2)-(self.cursor.rect.height/2)

    def move_cursor(self,i):
        # i: 1 going down, -1 going up
        new_value = self.cursor.value + i
        if new_value < 0:
            new_value = len(self.buttons)-1
        if new_value >= len(self.buttons):
            new_value = 0
        self.set_cursor(new_value)

    def display_clean(self):
        otk = []
        for o in self.menu_obj:
            if o.__class__.__name__ == "Menuitem":
                otk.append(o)
                o.kill()
        for o in otk:
            self.menu_obj.remove(o)
        self.buttons = []

    def display_main(self):
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","xb":250,"h":380,"y":50}))
        x = self.menu_obj[-1].rect.left+12
        y = self.menu_obj[-1].rect.top+10
        a(Menuitem(self,
                   {"?":"text","text":"Bing the Robot","p":[x,y],"f":"22"}))
        y += 50
        a(Menuitem(self,
                   {"?":"button","text":"play","p":[x,y],"s":150}))
        y += 40

        e,cl = 0,(120,120,120)
        if len(self.core.profile.scores)>=3:
            e,cl = 1,(0,0,0)
        a(Menuitem(self,
                   {"?":"button","text":"practice","p":[x,y],"s":150,"e":e,"cl":cl}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"scores","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"profile","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"options","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"help","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"about","p":[x,y],"s":150}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"quit","p":[x,y],"s":150}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)

    def display_profile(self,page=0):
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","xb":100,"h":340,"y":80}))
        x = self.menu_obj[-1].rect.left+10
        y = self.menu_obj[-1].rect.top+10
        a(Menuitem(self,
                   {"?":"text","text":"The current profile is: %s"%self.core.profile.current_profile,"p":[x,y],"f":"18"}))
        y += 30
        a(Menuitem(self,
                   {"?":"text","text":"Profiles keep tracks of options, progression and scores","p":[x,y],"f":"18"}))
        y += 40

        di = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","profile")
        pn = [ os.path.splitext(f)[0] for f in os.listdir(di) if os.path.isfile(os.path.join(di,f)) and os.path.splitext(f)[1] == ".cfg" ]
        pn.sort()

        cpn1 = pn[page*5:]
        cpn2 = cpn1[:5]
        oy = y
        for l in cpn2:
            a(Menuitem(self,
                       {"?":"button","text":l,"value":"pro_%s"%l,"p":[x,y],"s":420}))
            y += 40
        y = oy+(5*40)
        a(Menuitem(self,
                   {"?":"button","text":"new","value":"proN","p":[x,y],"s":110}))
        if page > 0:
            a(Menuitem(self,
                       {"?":"button","text":"<","value":"proB_%i"%(page-1),"p":[x+140,y],"s":60}))            
        if len(cpn1)>5:
            a(Menuitem(self,
                       {"?":"button","text":">","value":"proB_%i"%(page+1),"p":[x+220,y],"s":60}))
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x+310,y],"s":110}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)

    def display_scores(self,page=0):
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","xb":150,"h":330,"y":80}))
        x = self.menu_obj[-1].rect.left+10
        y = self.menu_obj[-1].rect.top+10
        a(Menuitem(self,
                   {"?":"text","text":"Scores:","p":[x,y],"f":"18"}))
        y += 30
        
        pn = self.core.profile.scores[:]
        cpn1 = pn[page*8:]
        cpn2 = cpn1[:8]
        oy = y
        for l in cpn2:
            t1 = l.split()[2]
            t2 = l.split()[0]
            t3 = l.split()[1]
            a(Menuitem(self,
                       {"?":"text","text":t1,"p":[x,y],"f":"18"}))
            a(Menuitem(self,
                       {"?":"text","text":t2,"p":[x+170,y],"f":"18"}))
            a(Menuitem(self,
                       {"?":"text","text":t3,"p":[x+310,y],"f":"18"}))
            y += 30
        y = oy+(8*30)+10
        if page > 0:
            a(Menuitem(self,
                       {"?":"button","text":"<","value":"scoB_%i"%(page-1),"p":[x,y],"s":60}))            
        if len(cpn1)>8:
            a(Menuitem(self,
                       {"?":"button","text":">","value":"scoB_%i"%(page+1),"p":[x+80,y],"s":60}))
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x+160,y],"s":100}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)

    def display_options(self,change=""):
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","xb":222,"h":230,"y":140}))
        x = self.menu_obj[-1].rect.left+10
        y = self.menu_obj[-1].rect.top+10
        a(Menuitem(self,
                   {"?":"text","text":"Options:","p":[x,y],"f":"18"}))
        y += 50

        cp = 0
        if change == "s1":
            self.core.profile.sound1 = (self.core.profile.sound1+1)%6
            self.display.change_volume_music()
        if change == "s2":
            cp = 1
            self.core.profile.sound2 = (self.core.profile.sound2+1)%6
        if change == "fs":
            cp = 2
            self.core.profile.fullscreen = (self.core.profile.fullscreen+1)%2
            self.display.set_fullscreen(self.core.profile.fullscreen)
            self.display.redraw()

        s1 = self.core.profile.sound1
        s2 = self.core.profile.sound2
        music_text = (s1*"#")+((5-s1)*"_")
        sound_text = (s2*"#")+((5-s2)*"_")

        a(Menuitem(self,
                   {"?":"button","text":"music: [%s]"%(music_text),"value":"optB_s1","p":[x,y],"s":200}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"sound: [%s]"%(sound_text),"value":"optB_s2","p":[x,y],"s":200}))
        y += 40
        if self.core.profile.fullscreen:
            a(Menuitem(self,
                       {"?":"button","text":"fullscreen ON","value":"optB_fs","p":[x,y],"s":200}))
        else:
            a(Menuitem(self,
                       {"?":"button","text":"fullscreen OFF","value":"optB_fs","p":[x,y],"s":200}))
        y += 40

        if change:
            self.core.profile.save()

        y += 10
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x,y],"s":100}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(cp,silent=1)


    def display_about(self):
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","xb":150,"h":340,"y":80}))
        x = self.menu_obj[-1].rect.left+10
        y = self.menu_obj[-1].rect.top+10
        a(Menuitem(self,
                   {"?":"text","text":"Bing the Robot","p":[x,y],"f":"22"}))
        y += 50
        lot = ["Created for pyweek24",
               "https://pyweek.org",
               "October 21st 2017","",
               "by Cauch","",
               "GNU GPL3 licence, sources available here:",
               "https://gitlab.com/cauch/bing_the_robot"]
        for l in lot:
            if l:
                a(Menuitem(self,
                           {"?":"text","text":l,"p":[x,y],"f":"18"}))
                y += 30
            else:
                y += 20
        y += 10
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x,y],"s":100}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)

    def display_help(self):
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","xb":170,"h":400,"y":50}))
        x = self.menu_obj[-1].rect.left+10
        y = self.menu_obj[-1].rect.top+10
        a(Menuitem(self,
                   {"?":"text","text":"Bing the Robot","p":[x,y],"f":"22"}))
        y += 50
        lot = ["control:",
               " arrow keys, or WASD or ZQSD",
               " 'ESC' or 'p' to pause",
               " 'r' to reload the level",
               "",
               "game play:",
               " get the robot outside the screen",
               " trying to get the higher score",
               " (be quick and take the shortest path)",
               " and avoiding getting stuck or worse",
           ]
        for l in lot:
            if l:
                a(Menuitem(self,
                           {"?":"text","text":l,"p":[x,y],"f":"18"}))
                y += 30
            else:
                y += 20
        y += 10
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x,y],"s":100}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)

    def end(self):
        self.bkgd = None
        self.bkgd_first = 0

        self.position = "main"

        self.menu_sprite = None
        self.menu_obj = []

        self.buttons = []
        self.cursor = None

