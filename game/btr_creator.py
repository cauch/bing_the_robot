#!/usr/bin/env python3

#    This file is part of Bing_the_robot.
#
#    Bing_the_robot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Bing_the_robot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Bing_the_robot.  If not, see <http://www.gnu.org/licenses/>.

import sys
from tkinter import *

from btr_generator import Generator

print("keys:")
print("a: next click will reset the entrance")
print("z: next click will reset the exit")
print("s: save the current status")
print("b: create borders")
print("x: turn on and off the developer tools")
print("o: solve and display the solution")
print("g: generate a new map")
print("c: remove the useless bloc")


class creator:
    def __init__(self):
        self.show_dev = 1
        self.loaded = {}
        self.loaded["walls"] = []
        self.loaded["p_in"] = [0,10]
        self.loaded["p_out"] = [39,10]
        self.size_h = 29
        self.size_w = 40
        self.o = []
        self.click_mode = "wall"
        self.filename = "temp"
        self.g = None

    def run(self):
        self.root=Tk()
        frame=Frame(self.root,width=800,height=600)
        frame.grid(row=0,column=0)
        self.canvas=Canvas(frame,bg='#FFFFFF',width=800,height=600)
        self.canvas.bind("<Button-1>", self.button1)
        #self.canvas.bind("<Button-3>", self.button3)
        self.canvas.bind("<Key>", self.key)
        self.root.bind("<Key>", self.key)
        self.canvas.pack(side=LEFT,expand=True,fill=BOTH)
        self.draw()
        self.root.mainloop()

    def load(self,filename):
        self.filename = filename.split("/")[-1].split(".")[-2]
        f = open(filename)
        d = f.readlines()
        for dd in d:
            t = dd.strip()
            if 'self.loaded' in t:
                exec(t)

    def button1(self,evt):
        x,y = int(evt.x/20),int(evt.y/20)-1
        if y < 0:
            return
        if self.click_mode == "wall":
            if [x,y] in self.loaded["walls"]:
                self.loaded["walls"].remove([x,y])
            else:
                self.loaded["walls"].append([x,y])
        if self.click_mode == "in":
            self.loaded["p_in"] = [x,y]
        if self.click_mode == "out":
            self.loaded["p_out"] = [x,y]
        self.click_mode = "wall"
        self.draw()

    def add_borders(self):
        walls = []
        walls += [ [0,x] for x in range(self.size_h) ]
        walls += [ [x,0] for x in range(1,self.size_w-1) ]
        walls += [ [self.size_w-1,x] for x in range(self.size_h) ]
        walls += [ [x,self.size_h-1] for x in range(1,self.size_w-1) ]
        for l in walls:
            if l not in self.loaded["walls"]:
                self.loaded["walls"].append(l)

    def key(self,evt):
        if evt.keycode == 39:
            self.save()
        elif evt.keycode == 56:
            self.add_borders()
        elif evt.keycode == 24:
            self.click_mode = "in"
        elif evt.keycode == 25:
            self.click_mode = "out"
        elif evt.keycode == 53:
            self.show_dev = (self.show_dev+1)%2
        elif evt.keycode == 32:
            self.solve()
        elif evt.keycode == 42:
            self.generate()
        elif evt.keycode == 54:
            self.clean_wall()
        else:
            print("keycode",evt.keycode)
        self.draw()

    def draw(self):
        dx,dy = 0,20
        for l in self.o:
            self.canvas.delete(l)
        self.o.append( self.canvas.create_rectangle(dx,dy,dx+800,dy+600, width=1, outline='black') )
        for l in self.loaded["walls"]:
            x,y = l[0]*20,l[1]*20
            self.o.append( self.canvas.create_rectangle(x+dx,y+dy,x+dx+20,y+dy+20, width=0, fill='grey') )
            self.o.append( self.canvas.create_text(x+dx+11,y+dy+9,text=str(l),anchor=NW))
        if self.show_dev != 0:
            for l in self.loaded.get("stop",[]):
                x,y = l[0]*20,l[1]*20
                self.o.append( self.canvas.create_rectangle(x+dx+2,y+dy+2,x+dx+20-2,y+dy+20-2, width=0, fill='green') )
            for l in self.loaded.get("free",[]):
                x,y = l[0]*20,l[1]*20
                self.o.append( self.canvas.create_rectangle(x+dx+4,y+dy+4,x+dx+20-4,y+dy+20-4, width=0, fill='yellow') )
            for l in self.loaded.get("fake",[]):
                x,y = l[0]*20,l[1]*20
                self.o.append( self.canvas.create_rectangle(x+dx,y+dy+16,x+dx+20,y+dy+20, width=0, fill='purple') )
            for l in self.loaded.get("walls_u",[]):
                x,y = l[0]*20,l[1]*20
                self.o.append( self.canvas.create_rectangle(x+dx+8,y+dy+8,x+dx+20-8,y+dy+20-8, width=0, fill='grey80') )
        x,y = self.loaded["p_in"][0]*20,self.loaded["p_in"][1]*20
        self.o.append( self.canvas.create_rectangle(x+dx+6,y+dy+6,x+dx+20-6,y+dy+20-6, width=0, fill='red') )
        x,y = self.loaded["p_out"][0]*20,self.loaded["p_out"][1]*20
        self.o.append( self.canvas.create_rectangle(x+dx+6,y+dy+6,x+dx+20-6,y+dy+20-6, width=0, fill='darkred') )
        for l in self.loaded.get("solution",[]):
            x,y = l[0]*20,l[1]*20
            self.o.append( self.canvas.create_rectangle(x+dx+8,y+dy+8,x+dx+20-8,y+dy+20-8, width=0, fill='blue') )

    def mprint(self,t):
        print(t)
        self.sfile.write("%s\n"%t)

    def save(self):
        lts = ["walls","stop","free","fake","p_in","p_out"]
        self.sfile = open("tmp_%s.txt"%self.filename,"w")
        for ll in lts:
            self.mprint('self.loaded["%s"]=%s'%(ll,repr(self.loaded.get(ll,[]))))
        if "length" not in self.loaded.keys():
            self.solve()
        self.mprint('self.loaded["length"]=%i'%self.loaded["length"])
        p_in = self.loaded["p_in"]
        if p_in[0] == 0:
            self.mprint('self.loaded["dire"]=0')
        elif p_in[1] == 0:
            self.mprint('self.loaded["dire"]=2')
        elif p_in[0] == self.size_w-1:
            self.mprint('self.loaded["dire"]=1')
        elif p_in[1] == self.size_h-1:
            self.mprint('self.loaded["dire"]=3')
        self.sfile.close()

    def solve(self):
        if self.g == None:
            self.g = Generator()
        r1 = self.g.solve(self.loaded["walls"][:],self.loaded["p_in"],self.loaded["p_out"])
        self.loaded["solution"] = r1
        self.loaded["length"] = self.g.s_length

    def clean_wall(self):
        if self.g == None:
            self.g = Generator()
        r1 = self.g.solve(self.loaded["walls"][:],self.loaded["p_in"],self.loaded["p_out"])
        l = []
        for ll in self.g.s_wall_touched:
            if ll not in l and ll != self.g.p_in:
                l.append(ll)
        self.loaded["walls_u"] = l

    def generate(self):
        if self.g == None:
            self.g = Generator()
        self.g.verbose = 1
        self.g.non_breaking = 1
        i = self.g.generate()
        if i:
            self.loaded["walls"]=self.g.wall
            self.loaded["stop"]=self.g.stop
            self.loaded["free"]=self.g.free
            self.loaded["fake"]=self.g.fake
            self.loaded["p_in"]=self.g.p_in
            self.loaded["p_out"]=self.g.p_out
        self.g.verbose = 0
        self.g.non_breaking = 0


if __name__ == '__main__':
    c = creator()
    if len(sys.argv) > 1:
        c.load(sys.argv[1])
    c.run()
