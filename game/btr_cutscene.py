#! /usr/bin/env python3

#    This file is part of Bing_the_robot.
#
#    Bing_the_robot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Bing_the_robot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Bing_the_robot.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

class CSitem(pygame.sprite.Sprite):
    def __init__(self,cs,options):
        """
        Sprite corresponding to an elemet of the menu
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.cs = cs
        self.options = options
        if options.get("?") == "bkgd":
            #background of the panel
            w = options.get("w",100)
            h = options.get("h",100)
            self.image = pygame.Surface((w,h)).convert_alpha()
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
            self.image.fill((255, 255, 255, 100))
        if options.get("?") == "bubble":
            #background of the panel
            w = options.get("w",100)
            h = options.get("h",100)
            lh = options.get("lh",100)
            ld = options.get("ld",100)
            self.image = pygame.Surface((w,h+lh)).convert_alpha()
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
            cl = (255, 255, 255)
            pygame.draw.circle(self.image,cl,(20,20),20)
            pygame.draw.circle(self.image,cl,(w-20,20),20)
            pygame.draw.circle(self.image,cl,(20,h-20),20)
            pygame.draw.circle(self.image,cl,(w-20,h-20),20)
            pygame.draw.rect(self.image,cl,pygame.Rect(20,0,w-40,h))
            pygame.draw.rect(self.image,cl,pygame.Rect(0,20,w,h-40))
            pygame.draw.polygon(self.image,cl,[((w/2)-10,h), ((w/2)+ld,h+lh), ((w/2)+10,h) ])
            self.image.fill((255, 255, 255, 100), None, pygame.BLEND_RGBA_MULT)
        if options.get("?") == "text":
            self.cl = options.get("cl",(255,255,255))
            self.text = options.get("text","")
            f = options.get("f","18")
            self.image = cs.display.fonts[f].render( self.text, 1, self.cl)
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
        if options.get("?") == "img":
            #background of the panel
            name = options.get("n","icon")
            self.image = cs.display.images[name]
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))

    def refresh(self):
        self.cs.cs_sprite.move_to_front(self)
        return 1

class BtrCutscene:
    def __init__(self,display):
        self.display = display
        self.core = display.core
        self.size = display.size

        self.bkgd = None
        self.bkgd_first = 0

        self.cs_sprite = None
        self.cs_obj = []

        self.scene_list = []

        self.next_level = "random"

    def init(self):

        self.cs_sprite = pygame.sprite.LayeredUpdates()
        CSitem.containers = self.cs_sprite

        self.set_background()
        self.display.screen.blit(self.bkgd, (0,0))

        self.next_scene()

    def set_background(self):
        if self.bkgd:
            return
        bg = None
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        self.bkgd.fill((0,0,0))

    def check_key(self,key):
        pass

    def next_scene(self):
        if not self.scene_list:
            cm = self.display.module["play"]
            cm.level_name = self.next_level
            self.core.new_game()
        else:
            #erase all
            for o in self.cs_obj:
                o.kill()
            self.cs_obj = []
            #draw
            scene = self.scene_list[0]
            self.scene_list = self.scene_list[1:]
            a = self.cs_obj.append
            for ss in scene:
                a(CSitem(self,ss))
            ss = {"?":"text","text":"click 'space' or 'return'","p":[(self.display.screen_width/2)-100,self.display.screen_height-20],"f":"12"}
            a(CSitem(self,ss))

    def check_events(self,event):
        if event.type == ACTIVEEVENT:
            pass
        elif event.type == KEYDOWN:
            key = event.key
            if event.key == K_SPACE or event.key == K_RETURN:
                self.next_scene()

    def update(self):
        dirty = []
        redraw_cs = 0
        for o in self.cs_obj:
            r = o.refresh()
            if r:
                redraw_cs = 1
        
        if redraw_cs and self.cs_sprite:
            self.cs_sprite.clear(self.display.screen, self.bkgd)
            dirty.append(self.cs_sprite.draw(self.display.screen))

        redraw = 0
        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def end(self):
        self.bkgd = None
        self.bkgd_first = 0

        self.position = "main"

        self.menu_sprite = None
        self.menu_obj = []

        self.buttons = []
        self.cursor = None

