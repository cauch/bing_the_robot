#! /usr/bin/env python

#    This file is part of Bing_the_robot.
#
#    Bing_the_robot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Bing_the_robot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Bing_the_robot.  If not, see <http://www.gnu.org/licenses/>.

import os
import datetime

class Profile:
    def __init__(self,core):
        self.core = core
        self.display = core.display

        self.current_profile = "player01"

        #options
        self.sound1 = 2
        self.sound2 = 1
        self.fullscreen = 0
        self.scores = []
        self.lastlevel = ""
        self.currentscore = 0

    def create_new(self):
        di = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","profile")
        pn = [ os.path.splitext(f)[0] for f in os.listdir(di) if os.path.isfile(os.path.join(di,f)) and os.path.splitext(f)[1] == ".cfg" ]
        i = 0
        n = None
        while 1:
            i+=1
            n = "player%02i"%i
            if n not in pn:
                break
        self.save(n)
        return n

    def load(self):
        filename= os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               "..","save.cfg")
        if not os.path.exists(filename):
            fi = open(filename,"w")
            n = self.create_new()
            fi.write("profile %s\n"%n)
            fi.close()
        fi = open(filename,"r")
        da = fi.readlines()
        for d in da:
            t = d.strip()
            if t.startswith("profile"):
                tt = t.split()[-1]
                self.current_profile = tt
        self.load_profile()

    def load_profile(self):
        profile_name = self.current_profile
        filename= os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               "..","profile","%s.cfg"%profile_name)
        if not os.path.exists(filename):
            self.save(profile_name)

        fi = open(filename,"r")
        da = fi.readlines()
        self.scores = []
        for d in da:
            t = d.strip()
            if t.startswith("sound1"):
                self.sound1 = int(t.split()[1])
            if t.startswith("sound2"):
                self.sound2 = int(t.split()[1])
            if t.startswith("fullscreen"):
                self.fullscreen = int(t.split()[1])
            if t.startswith("currentscore"):
                self.currentscore = int(t.split()[1])
            if t.startswith("score"):
                self.scores.append(" ".join(t.split()[1:]))
            if t.startswith("lastlevel"):
                self.lastlevel = " ".join(t.split()[1:])

    def change_profile(self,pn):
        filename= os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               "..","save.cfg")
        fi = open(filename,"w")
        self.current_profile = pn
        fi.write("profile %s\n"%pn)
        fi.close()
        self.load()
        self.core.display.change_volume_music()

    def save(self,n=""):
        filename = "%s.cfg"%self.current_profile
        if n:
            filename = "%s.cfg"%n
        tfilename= os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                "..","profile",filename)
        fi = open(tfilename,"w")
        fi.write("sound1 %i\n"%self.sound1)
        fi.write("sound2 %i\n"%self.sound2)
        fi.write("fullscreen %i\n"%self.fullscreen)
        fi.write("lastlevel %s\n"%self.lastlevel)        
        fi.write("currentscore %s\n"%self.currentscore)
        for s in self.scores:
            fi.write("score %s\n"%s)
        fi.close()

    def add_score(self,level_name,point=0):
        added = 0
        time = datetime.datetime.now().strftime("%y-%m-%d-%H-%M")
        text = "%s %i %s"%(level_name,point,time)
        for i in range(len(self.scores)):
            if level_name == self.scores[i].split()[0]:
                added = 1
                os = int(self.scores[i].split()[1])
                if point < os:
                    added = 2
                    self.scores[i] = text
        if added == 0:
            self.scores.append(text)
            added = 2
        if added == 2:
            self.save()
